import 'package:MyPlanning/const/database.dart';
import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/models/Users.models.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreServices {
  Firestore _db = Firestore.instance;

  static final FirestoreServices _singleton = new FirestoreServices._internal();

  factory FirestoreServices() {
    return _singleton;
  }

  FirestoreServices._internal();

  Future<List<ProjectModel>> listProject() async {
    var elements =
        await _db.collection(DatabaseConst.PROJECTS_TABLE_NAME).getDocuments();
    return elements.documents
        .map((DocumentSnapshot d) => ProjectModel.fromSnapshot(d))
        .toList();
  }

  Future<bool> deleteProject(ProjectModel item) async {
    try {
      await _db
          .collection(DatabaseConst.PROJECTS_TABLE_NAME)
          .document(item.id)
          .delete();
      return true;
    } catch (e) {
      return await Future.value(false);
    }
  }

  Future<bool> createProject(ProjectModel item) async {
    try {

      Map<String, dynamic> dataToCreate = item.toJson();
      //remove key or id entity from my json map
      dataToCreate.remove('id');

      await _db
          .collection(DatabaseConst.PROJECTS_TABLE_NAME)
          .add(dataToCreate);
      return true;
    } catch (e) {
      return await Future.value(false);
    }
  }

  Future<bool> updateProject(ProjectModel item) async {
    try {
      final DocumentSnapshot document = await _db
          .collection(DatabaseConst.PROJECTS_TABLE_NAME)
          .document(item.id)
          .snapshots()
          .first;

      item.updatedAt = new DateTime.now();

      Map<String, dynamic> dataToUpdate = item.toJson();
      //remove key or id entity from my json map
      dataToUpdate.remove('id');

      await document.reference.updateData(dataToUpdate);

      return true;
    } catch (e) {
      return await Future.value(false);
    }
  }

  Future<bool> setProfile(UserModel item) async {
    try {

      Map<String, dynamic> dataToUpdate = item.toJson();
      //remove key or id entity from my json map
      dataToUpdate.remove('id');

      await _db
          .collection(DatabaseConst.USERS_TABLE_NAME)
          .document(item.id)
          .setData(dataToUpdate, merge: true);
      return true;
    } catch (e) {
      return await Future.value(false);
    }
  }


  // Users CRUD

  Future<List<UserModel>> listUsers() async {
    var elements =
    await _db.collection(DatabaseConst.USERS_TABLE_NAME).getDocuments();
    return elements.documents
        .map((DocumentSnapshot d) => UserModel.fromSnapshot(d))
        .toList();
  }

  Future<bool> deleteUsers(UserModel item) async {
    try {
      await _db
          .collection(DatabaseConst.USERS_TABLE_NAME)
          .document(item.id)
          .delete();
      return true;
    } catch (e) {
      return await Future.value(false);
    }
  }

  Future<bool> createUsers(UserModel item) async {
    try {

      Map<String, dynamic> dataToCreate = item.toJson();
      //remove key or id entity from my json map
      dataToCreate.remove('id');

      await _db
          .collection(DatabaseConst.USERS_TABLE_NAME)
          .add(dataToCreate);
      return true;
    } catch (e) {
      return await Future.value(false);
    }
  }

  Future<bool> updateUsers(UserModel item) async {
    try {
      final DocumentSnapshot document = await _db
          .collection(DatabaseConst.USERS_TABLE_NAME)
          .document(item.id)
          .snapshots()
          .first;

      Map<String, dynamic> dataToUpdate = item.toJson();
      //remove key or id entity from my json map
      dataToUpdate.remove('id');

      await document.reference.updateData(dataToUpdate);

      return true;
    } catch (e) {
      return await Future.value(false);
    }
  }
}
