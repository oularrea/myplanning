
import 'package:MyPlanning/models/Users.models.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthServices {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  static final AuthServices _singleton = new AuthServices._internal();

  factory AuthServices() {
    return _singleton;
  }

  AuthServices._internal();

  Future<bool> checkLogin() async {

    return Future.value((await _auth.currentUser() != null));

  }

  Future<FirebaseUser> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn().catchError((onError) {
      print("Error $onError");
    });

    if(googleSignInAccount == null){
      return null;
    }

    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;


    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    if(user.isAnonymous){
      return null;
    }

    if(await user.getIdToken() == null){
      return null;
    }

    final FirebaseUser currentUser = await _auth.currentUser();

    if(user.uid != currentUser.uid){
      return null;
    }
    return currentUser;
  }

  void signOutGoogle() async{
    await googleSignIn.signOut();
    await _auth.signOut();

    print("User Sign Out");
  }

}
