import 'package:MyPlanning/enviroment.dart';
import 'package:MyPlanning/screens/projects/FormProject.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:flutter/material.dart';
import 'package:MyPlanning/const/navigation.dart';
import 'package:MyPlanning/screens/HomeScreen.dart';
import 'package:MyPlanning/screens/LandingScreen.dart';
import 'package:MyPlanning/screens/LoginScreen.dart';
import 'package:MyPlanning/screens/projects/ViewProjectScreen.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'config/theme.dart';

void main() async {

  Store<AppState> store = await Environment.setup();
  runApp(MyApp(store));
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  MyApp(this.store);

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
          debugShowCheckedModeBanner: true,
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            Locale('es'),
            Locale('en'),
          ],
          title: 'My Planning',
          theme: myTheme,
          themeMode: ThemeMode.system,
          darkTheme: ThemeData.dark(),
          initialRoute: '/',
          routes: {
            '/': (context) => Landing(),
            '/login': (context) => Login(),
            '/home': (context) => MyHomePage(store: store),
            '/view-project': (context) => ViewProject(),
            '/form-project': (context) => FormProject()
          },
          navigatorKey: NavigationConst.navKey
          //home: MyHomePage(title: "Hola")
          ),
    );
  }
}
