import 'package:flutter/material.dart';

class CustomChip extends StatelessWidget {

    final String label;
    final bool selected;
    final Function(bool selected) onSelect;

    CustomChip(
        this.label,
        this.selected,
        this.onSelect,
        { Key key }
        ) : super(key: key);

    @override
    Widget build(BuildContext context) {
        return AnimatedContainer(
            height: 100,
            width: 70,
            margin: EdgeInsets.symmetric(
                vertical: 15,
                horizontal: 5,
            ),
            duration: Duration(milliseconds: 300),
            decoration: BoxDecoration(
                color: selected ? Colors.green : Colors.transparent,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(
                    color: selected ? Colors.green : Colors.grey,
                    width: 1,
                ),
            ),
            child: InkWell(
                onTap: () => onSelect(!selected),
                child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                        Visibility(
                            visible: selected,
                            child: Icon(
                                Icons.check_circle_outline,
                                color: Colors.white,
                                size: 32,
                            )
                        ),
                        Positioned(
                            left: 9,
                            right: 9,
                            bottom: 7,
                            child: Container(
                                child: Text(
                                    label,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: selected ? Colors.white : Colors.black45,
                                    ),
                                ),
                            ),
                        ),
                    ],
                ),
            ),
        );
    }
}