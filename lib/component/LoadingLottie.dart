import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoadingLottie extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Lottie.asset('assets/animations/loading.json', width: 75,
          height: 75,
          fit: BoxFit.fill),
    );
  }
}
