import 'package:MyPlanning/component/SwipeDetectorVertical.dart';
import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  final GestureTapCallback onTap;
  final Function() onPointerDow;
  final Function() onPointerUp;
  final Widget content;

  CircleButton(
      {Key key, this.onTap, this.content, this.onPointerDow, this.onPointerUp})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // double size = 35.0;
    return SwipeDetectorVertical(
      onSwipeDown: onPointerDow,
      onSwipeUp: onPointerUp,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: InkResponse(
          onTap: onTap,
          child: Container(
              //width: size,
              //height: size,
              decoration: BoxDecoration(
                color: Colors.transparent,
                shape: BoxShape.circle,
              ),
              child: Center(child: content)),
        ),
      ),
    );
  }
}
