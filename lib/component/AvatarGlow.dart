import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';

class AnimateLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: AvatarGlow(
        endRadius: 90,
        duration: Duration(seconds: 2),
        glowColor: Colors.white24,
        repeat: true,
        repeatPauseDuration: Duration(seconds: 2),
        startDelay: Duration(seconds: 1),
        child: Material(
            elevation: 8.0,
            shape: CircleBorder(),
            child: CircleAvatar(
              backgroundColor: Colors.grey[100],
              child: Image.asset(
                "assets/icon/logo.png",
                height: 60.0,
                width: 60.0,
              ),
              radius: 50.0,
            )),
      )
    );
  }
}
