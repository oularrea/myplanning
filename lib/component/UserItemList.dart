import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

final Color discountBackgroundColor = Color(0xFFFFE08D);

class UsersItem extends StatelessWidget {
  final UserModel userModel;
  final Function(UserModel) onView;

  UsersItem({Key key, @required this.userModel, this.onView});

  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, int>(
        distinct: true,
        rebuildOnChange: true,
        converter: (store) => store.state.filtersState.month,
        builder: (_, month) {
          return InkWell(
            onTap: () => onView(userModel),
            child: ListTile(
              leading: Stack(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: NetworkImage(userModel.photoUrl),
                  ),
                  Positioned(
                    bottom: 1,
                    right: 0,
                    width: 12,
                    height: 12,
                    child: Container(
                      decoration: BoxDecoration(
                        color: userModel.color,
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                  ),
                ],
              ),
              title: Text(userModel.displayName),
              subtitle: Text(userModel.email),
              trailing: Icon(
                Icons.more_vert,
              ),
              onTap: () {
                  nextFeature(context);
              },
            )
          );
        });
  }

  void nextFeature(BuildContext context){
      Flushbar<bool>(
          message: "Próximamente se implementarán las funcionalidades",
          icon: Icon(
              Icons.new_releases,
              size: 28.0,
              color: Colors.blue[300],
          ),
          duration: Duration(seconds: 3),
          isDismissible: true,
          showProgressIndicator: false,
          progressIndicatorBackgroundColor: Colors.blueGrey,
          flushbarPosition: FlushbarPosition.BOTTOM,
          flushbarStyle: FlushbarStyle.GROUNDED,
          reverseAnimationCurve: Curves.decelerate,
          forwardAnimationCurve: Curves.bounceIn,
          animationDuration: Duration(milliseconds: 500)
      )..show(context);
  }
}