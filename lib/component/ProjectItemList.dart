import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/screens/projects/ViewProjectScreen.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

final Color discountBackgroundColor = Color(0xFFFFE08D);

class ProjectsItem extends StatelessWidget {
    final ProjectModel projectModel;
    final Function(ProjectModel) onView;
    final ContainerTransitionType _transitionType = ContainerTransitionType.fade;

    ProjectsItem({Key key, @required this.projectModel, this.onView});

    @override
    Widget build(BuildContext context) {

        return StoreConnector<AppState, int>(
            distinct: true,
            rebuildOnChange: true,
            converter: (store) => store.state.filtersState.month,
            builder: (_, month) {
                return OpenContainer(
                    transitionType: _transitionType,
                    transitionDuration: new Duration(milliseconds: 300),
                    openBuilder: (BuildContext _, VoidCallback openContainer) {
                        return ViewProject();
                    },
                    tappable: false,
                    closedShape: const RoundedRectangleBorder(),
                    closedElevation: 0.0,
                    closedBuilder: (BuildContext _, VoidCallback openContainer) {
                        return InkWell(
                            onTap: (){
                                onView(projectModel);
                                return openContainer();
                            },
                            child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: Stack(
                                    children: <Widget>[
                                        Container(
                                            margin: const EdgeInsets.only(right: 16.0, left: 16.0),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10.0),
                                                ),
                                                border: Border.all(
                                                    color: Color(
                                                        int.parse(projectModel.shadeColor, radix: 16) +
                                                            0xFF000000)),
                                                color: Color(
                                                    int.parse(projectModel.shadeColor, radix: 16) +
                                                        0xFF000000),
                                            ),
                                            child: Padding(
                                                padding: const EdgeInsets.all(16.0),
                                                child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                        Row(
                                                            children: <Widget>[
                                                                Text(
                                                                    '${projectModel.name}',
                                                                    style: TextStyle(
                                                                        fontWeight: FontWeight.bold,
                                                                        fontSize: 20.0,
                                                                        color: Colors.white
                                                                    ),
                                                                ),
                                                                SizedBox(
                                                                    width: 4.0,
                                                                ),
                                                                Text(
                                                                    _getHourByMonth(projectModel, month),
                                                                    style: TextStyle(
                                                                        fontWeight: FontWeight.bold,
                                                                        fontSize: 16.0,
                                                                        decoration: TextDecoration.none,
                                                                        color: Colors.grey[400]),
                                                                ),
                                                            ],
                                                        ),
                                                        Wrap(
                                                            spacing: 8.0,
                                                            runSpacing: -8.0,
                                                            children: <Widget>[],
                                                        )
                                                    ],
                                                ),
                                            ),
                                        ),
                                        _renderSLA(projectModel)
                                    ],
                                ),
                            ),
                        );
                    }
                );
            });
    }
}

Widget _renderSLA(ProjectModel item) {
    if (item.maintenance != null && item.maintenance.isMaintenaince) {
        return Positioned(
            top: 10.0,
            right: 0.0,
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: Text(
                    'SLA',
                    style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
                ),
                decoration: BoxDecoration(
                    color: discountBackgroundColor,
                    borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                    ),
                ),
            ),
        );
    }
    return Container();
}

String _getHourByMonth(ProjectModel projectModel, int month){
    String response = "0";

    projectModel.planning.forEach((PlanningModel p) {
        if(p.date.month == month){
            response = p.total.toString();
            return;
        }
    });

    return "($response h)";
}
