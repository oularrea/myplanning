import 'package:MyPlanning/const/navigation.dart';
import 'package:MyPlanning/providers/firestore.services.dart';
import 'package:MyPlanning/store/actions/project.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> projectsMiddleware() {
  final FirestoreServices firestoreServices = new FirestoreServices();

  final requestProjects = _createLoadProjectsRequest(firestoreServices);

  final saveProjects = _saveProjectsRequest(firestoreServices);
  final updateProjects = _updateProjectsRequest(firestoreServices);

  return ([
    TypedMiddleware<AppState, LoadProjectsAction>(requestProjects),
    TypedMiddleware<AppState, SaveProjectAction>(saveProjects),
    TypedMiddleware<AppState, UpdateProjectAction>(updateProjects),
    TypedMiddleware<AppState, ViewProjectAction>(_viewProject),
    TypedMiddleware<AppState, EditProjectAction>(_editProject),
    TypedMiddleware<AppState, NewProjectAction>(_addProject)
  ]);
}

Middleware<AppState> _createLoadProjectsRequest(
    FirestoreServices firestoreServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      var projects = await firestoreServices.listProject();
      store.dispatch(new LoadProjectsSuccessAction(projects: projects));
    } catch (error) {
      print(error.toString());
      store.dispatch(new LoadProjectsFailureAction(error: error));
    }

    nextDispatcher(action);
  };
}

Middleware<AppState> _saveProjectsRequest(
    FirestoreServices firestoreServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      var onSave = await firestoreServices.createProject(action.project);
      store.dispatch(new UpdateProjectResponseAction(response: onSave));
      store.dispatch(new LoadProjectsAction());
      NavigationConst.navKey.currentState.pop();
    } catch (error) {
      store.dispatch(new LoadProjectsFailureAction(error: error));
    }
    nextDispatcher(action);
  };
}

Middleware<AppState> _updateProjectsRequest(
    FirestoreServices firestoreServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      var onSave = await firestoreServices.updateProject(action.project);
      store.dispatch(new UpdateProjectResponseAction(response: onSave));
      store.dispatch(new LoadProjectsAction());
      NavigationConst.navKey.currentState.pop();
    } catch (error) {
      print(error.toString());
      store.dispatch(new LoadProjectsFailureAction(error: error));
    }

    nextDispatcher(action);
  };
}

_viewProject(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  //NavigationConst.navKey.currentState.pushNamed('/view-project');
  nextDispatcher(action);
}

_editProject(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  NavigationConst.navKey.currentState.pushNamed('/form-project');
  nextDispatcher(action);
}

_addProject(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  nextDispatcher(action);
}
