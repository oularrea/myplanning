import 'package:MyPlanning/const/navigation.dart';
import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/providers/auth.services.dart';
import 'package:MyPlanning/providers/firestore.services.dart';
import 'package:MyPlanning/store/actions/auth.action.dart';
import 'package:MyPlanning/store/actions/filters.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> authMiddleware() {
  final AuthServices authServices = new AuthServices();
  final FirestoreServices firestoreServices = new FirestoreServices();

  final login  = _createLoginRequest(authServices);
  final logout  = _createLogoutRequest(authServices);
  final setUser = _createSetUserRequest(firestoreServices);


  return ([
    TypedMiddleware<AppState, LoginAction>(login),
    TypedMiddleware<AppState, LoginSuccesAction>(_loginSuccess),
    TypedMiddleware<AppState, LoginErrorAction>(_loginError),
    TypedMiddleware<AppState, LogoutAction>(logout),
    TypedMiddleware<AppState, SetUserProfile>(setUser),
  ]);
}

Middleware<AppState> _createLoginRequest(
    AuthServices authServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      store.dispatch(new ToggleLoadingAction());
      var user = await authServices.signInWithGoogle();

      if(user == null){
        store.dispatch(new LoginErrorAction(error: "Cancelado por el usuario"));
      }else{
        UserModel userModel = new UserModel();
        userModel.displayName = user.displayName;
        userModel.photoUrl = user.photoUrl;
        userModel.email = user.email;
        userModel.id = user.uid;

        if(user != null){
          store.dispatch(new SetUserProfile(user: userModel));
          store.dispatch(new SetUser(user: userModel));
        }

        store.dispatch(new LoginSuccesAction());

      }
    } catch (error) {
      print(error.toString());
      store.dispatch(new LoginErrorAction(error: error.toString()));
    }
    nextDispatcher(action);
  };
}

Middleware<AppState> _createLogoutRequest(
    AuthServices authServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      await authServices.signOutGoogle();
      NavigationConst.navKey.currentState.pushReplacementNamed('/login');

    } catch (error) {
      print(error.toString());
      NavigationConst.navKey.currentState.pushReplacementNamed('/login');
    }
    nextDispatcher(action);
  };
}

_loginSuccess(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  NavigationConst.navKey.currentState.pushReplacementNamed('/home');
  nextDispatcher(action);
}

_loginError(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  nextDispatcher(action);
}

Middleware<AppState> _createSetUserRequest(
    FirestoreServices firestoreServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      await firestoreServices.setProfile(action.user);

    } catch (error) {
      print(error.toString());
      store.dispatch(new LoginErrorAction(error: error));
    }
    nextDispatcher(action);
  };
}