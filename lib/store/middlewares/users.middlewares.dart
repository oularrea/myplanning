import 'package:MyPlanning/const/navigation.dart';
import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/providers/firestore.services.dart';
import 'package:MyPlanning/store/actions/user.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> usersMiddleware() {
  final FirestoreServices firestoreServices = new FirestoreServices();
  final requestUsers = _createLoadUsersRequest(firestoreServices);
  final saveUsers = _saveUsersRequest(firestoreServices);
  final updateUsers = _updateUsersRequest(firestoreServices);

  return ([
    TypedMiddleware<AppState, LoadUsersAction>(requestUsers),
    TypedMiddleware<AppState, SaveUserAction>(saveUsers),
    TypedMiddleware<AppState, UpdateUserAction>(updateUsers),
    TypedMiddleware<AppState, ViewUserAction>(_viewUser),
    TypedMiddleware<AppState, EditUserAction>(_editUser),
    TypedMiddleware<AppState, NewUserAction>(_editUser),
    TypedMiddleware<AppState, RemoveScheduleAction>(_removeUser)
  ]);
}

Middleware<AppState> _createLoadUsersRequest(
    FirestoreServices firestoreServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      var users = await firestoreServices.listUsers();
      store.dispatch(new LoadUsersSuccessAction(users: users));
    } catch (error) {
      print(error.toString());
      store.dispatch(new LoadUsersFailureAction(error: error));
    }

    nextDispatcher(action);
  };
}

Middleware<AppState> _saveUsersRequest(
    FirestoreServices firestoreServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      var onSave = await firestoreServices.createUsers(action.user);
      store.dispatch(new UpdateUserResponseAction(response: onSave));
      store.dispatch(new LoadUsersAction());
      NavigationConst.navKey.currentState.pop();
    } catch (error) {
      store.dispatch(new LoadUsersFailureAction(error: error));
    }
    nextDispatcher(action);
  };
}

Middleware<AppState> _updateUsersRequest(
    FirestoreServices firestoreServices) {
  return (Store<AppState> store, action, NextDispatcher nextDispatcher) async {
    try {
      var onSave = await firestoreServices.updateUsers(action.user);
      store.dispatch(new UpdateUserResponseAction(response: onSave));
      store.dispatch(new LoadUsersAction());
      //NavigationConst.navKey.currentState.pop();
    } catch (error) {
      print(error.toString());
      store.dispatch(new LoadUsersFailureAction(error: error));
    }

    nextDispatcher(action);
  };
}

_viewUser(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  NavigationConst.navKey.currentState.pushNamed('/view-project');
  nextDispatcher(action);
}

_editUser(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  //NavigationConst.navKey.currentState.pushNamed('/form-project');
  nextDispatcher(action);
}

_removeUser(Store<AppState> store, action, NextDispatcher nextDispatcher) {
  UserModel userModel = action.user;
  ScheduleModel scheduleModel = action.schedule;
  userModel.schedules.removeWhere((element) => (element.id == scheduleModel.id));
  store.dispatch(new UpdateUserAction(user: userModel));
  nextDispatcher(action);
}