import 'package:MyPlanning/models/Users.models.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auth.state.g.dart';

@JsonSerializable()
class AuthState {
  UserModel user;
  bool isLoggin;
  bool loading = false;

  // to force the change of state
  final String uuid;

  AuthState({this.user, this.isLoggin, this.loading, this.uuid});

  AuthState.initialState()
      : user = null,
        isLoggin = false,
        loading = false,
        uuid = '';

  AuthState copyWith({u, l, uuid, lo}) => new AuthState(
      user: u ?? user, isLoggin: l ?? isLoggin, uuid: uuid ?? this.uuid, loading: lo ?? this.loading );

  factory AuthState.fromJson(Map<String, dynamic> json) => _$AuthStateFromJson(json);

  Map<String, dynamic> toJson() => _$AuthStateToJson(this);
}
