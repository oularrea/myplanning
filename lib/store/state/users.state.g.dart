// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users.state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UsersState _$UsersStateFromJson(Map<String, dynamic> json) {
  return UsersState(
    users: (json['users'] as List)
        ?.map((e) =>
            e == null ? null : UserModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    selected: json['selected'] == null
        ? null
        : UserModel.fromJson(json['selected'] as Map<String, dynamic>),
    uuid: json['uuid'] as String,
  );
}

Map<String, dynamic> _$UsersStateToJson(UsersState instance) =>
    <String, dynamic>{
      'users': instance.users,
      'selected': instance.selected,
      'uuid': instance.uuid,
    };
