// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app.state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppState _$AppStateFromJson(Map<String, dynamic> json) {
  return AppState(
    projectsState: json['projectsState'] == null
        ? null
        : ProjectsState.fromJson(json['projectsState'] as Map<String, dynamic>),
    filtersState: json['filtersState'] == null
        ? null
        : FiltersState.fromJson(json['filtersState'] as Map<String, dynamic>),
    authState: json['authState'] == null
        ? null
        : AuthState.fromJson(json['authState'] as Map<String, dynamic>),
    usersState: json['usersState'] == null
        ? null
        : UsersState.fromJson(json['usersState'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AppStateToJson(AppState instance) => <String, dynamic>{
      'projectsState': instance.projectsState,
      'filtersState': instance.filtersState,
      'authState': instance.authState,
      'usersState': instance.usersState,
    };
