import 'package:MyPlanning/models/Users.models.dart';
import 'package:json_annotation/json_annotation.dart';

part 'filters.state.g.dart';

@JsonSerializable()
class FiltersState {
  UserModel user;
  int month;

  // to force the change of state
  final String uuid;

  FiltersState({this.user, this.month, this.uuid});

  FiltersState.initialState()
      : user = null,
        month = new DateTime.now().month,
        uuid = '';

  FiltersState copyWith({u, m, uuid}) => new FiltersState(
      user: u ?? user, month: m ?? month, uuid: uuid ?? this.uuid);

  factory FiltersState.fromJson(Map<String, dynamic> json) => _$FiltersStateFromJson(json);

  Map<String, dynamic> toJson() => _$FiltersStateToJson(this);
}
