import 'package:MyPlanning/models/Projects.model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'projects.state.g.dart';

@JsonSerializable()
class ProjectsState {
  List<ProjectModel> projects;
  ProjectModel selected;

  // to force the change of state
  final String uuid;

  ProjectsState({this.projects, this.selected, this.uuid});

  ProjectsState.initialState()
      : projects = [],
        selected = null,
        uuid = '';

  ProjectsState copyWith({p, s, uuid}) => new ProjectsState(
      projects: p ?? projects,
      selected: s ?? selected,
      uuid: uuid ?? this.uuid);

  factory ProjectsState.fromJson(Map<String, dynamic> json) => _$ProjectsStateFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectsStateToJson(this);
}
