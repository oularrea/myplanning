import 'package:MyPlanning/models/Users.models.dart';
import 'package:json_annotation/json_annotation.dart';

part 'users.state.g.dart';

@JsonSerializable()
class UsersState {
  List<UserModel> users;
  UserModel selected;

  // to force the change of state
  final String uuid;

  UsersState({this.users, this.selected, this.uuid});

  UsersState.initialState()
      : users = [],
        selected = null,
        uuid = '';

  UsersState copyWith({u, s, uuid}) => new UsersState(
      users: u ?? users,
      selected: s ?? selected,
      uuid: uuid ?? this.uuid);

  factory UsersState.fromJson(Map<String, dynamic> json) => _$UsersStateFromJson(json);

  Map<String, dynamic> toJson() => _$UsersStateToJson(this);
}
