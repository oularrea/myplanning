// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filters.state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FiltersState _$FiltersStateFromJson(Map<String, dynamic> json) {
  return FiltersState(
    user: json['user'] == null
        ? null
        : UserModel.fromJson(json['user'] as Map<String, dynamic>),
    month: json['month'] as int,
    uuid: json['uuid'] as String,
  );
}

Map<String, dynamic> _$FiltersStateToJson(FiltersState instance) =>
    <String, dynamic>{
      'user': instance.user,
      'month': instance.month,
      'uuid': instance.uuid,
    };
