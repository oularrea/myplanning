import 'package:MyPlanning/store/state/auth.state.dart';
import 'package:MyPlanning/store/state/filters.state.dart';
import 'package:MyPlanning/store/state/users.state.dart';
import 'package:flutter/foundation.dart';
import 'package:MyPlanning/store/state/projects.state.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app.state.g.dart';

@JsonSerializable()
class AppState {
  final ProjectsState projectsState;
  final FiltersState filtersState;
  final AuthState authState;
  final UsersState usersState;

  AppState({@required this.projectsState, @required this.filtersState, @required this.authState, @required this.usersState});

  AppState.initialState({projectsState, filtersState, authState, usersState})
      : projectsState = projectsState ?? ProjectsState.initialState(),
        filtersState = filtersState ?? FiltersState.initialState(),
        authState = authState ?? AuthState.initialState(),
        usersState = usersState ?? UsersState.initialState()
  ;

  static AppState fromJson(dynamic json) {
    if(json == null){
      return null;
    }
    return _$AppStateFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AppStateToJson(this);

}
