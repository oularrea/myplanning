// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'projects.state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectsState _$ProjectsStateFromJson(Map<String, dynamic> json) {
  return ProjectsState(
    projects: (json['projects'] as List)
        ?.map((e) =>
            e == null ? null : ProjectModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    selected: json['selected'] == null
        ? null
        : ProjectModel.fromJson(json['selected'] as Map<String, dynamic>),
    uuid: json['uuid'] as String,
  );
}

Map<String, dynamic> _$ProjectsStateToJson(ProjectsState instance) =>
    <String, dynamic>{
      'projects': instance.projects,
      'selected': instance.selected,
      'uuid': instance.uuid,
    };
