import 'package:flutter/foundation.dart';
import 'package:MyPlanning/models/Projects.model.dart';

class LoadProjectsAction {}

class LoadProjectsSuccessAction {
  final List<ProjectModel> projects;

  LoadProjectsSuccessAction({@required this.projects});
}

class LoadProjectsFailureAction {
  final dynamic error;

  LoadProjectsFailureAction({@required this.error});
}

class DeleteProjectAction {
  final ProjectModel project;

  DeleteProjectAction({@required this.project});
}

class ViewProjectAction {
  final ProjectModel project;

  ViewProjectAction({@required this.project});
}

class EditProjectAction {
  final ProjectModel project;

  EditProjectAction({@required this.project});
}

class NewProjectAction {
  final ProjectModel project;

  NewProjectAction({@required this.project});
}

class SaveProjectAction {
  final ProjectModel project;

  SaveProjectAction({@required this.project});
}

class UpdateProjectAction {
  final ProjectModel project;

  UpdateProjectAction({@required this.project});
}

class UpdateProjectResponseAction{
  final bool response;

  UpdateProjectResponseAction({@required this.response});
}