import 'package:MyPlanning/models/Users.models.dart';
import 'package:flutter/foundation.dart';

class LoginAction {}

class ToggleLoadingAction{}

class LoginSuccesAction {}

class LoginErrorAction {
  final String error;
  LoginErrorAction({@required this.error});
}

class LogoutAction {}

class SetUserProfile {
  final UserModel user;

  SetUserProfile({@required this.user});
}