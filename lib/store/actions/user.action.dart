import 'package:MyPlanning/models/Users.models.dart';
import 'package:flutter/foundation.dart';

class LoadUsersAction {}

class LoadUsersSuccessAction {
  final List<UserModel> users;

  LoadUsersSuccessAction({@required this.users});
}

class LoadUsersFailureAction {
  final dynamic error;

  LoadUsersFailureAction({@required this.error});
}

class DeleteUserAction {
  final UserModel user;

  DeleteUserAction({@required this.user});
}

class ViewUserAction {
  final UserModel user;

  ViewUserAction({@required this.user});
}

class EditUserAction {
  final UserModel user;

  EditUserAction({@required this.user});
}

class NewUserAction {
  final UserModel user;

  NewUserAction({@required this.user});
}

class SaveUserAction {
  final UserModel user;

  SaveUserAction({@required this.user});
}

class UpdateUserAction {
  final UserModel user;

  UpdateUserAction({@required this.user});
}

class RemoveScheduleAction {
  final ScheduleModel schedule;
  final UserModel user;

  RemoveScheduleAction({@required this.schedule, @required this.user});
}

class UpdateUserResponseAction{
  final bool response;

  UpdateUserResponseAction({@required this.response});
}