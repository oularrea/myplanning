import 'package:MyPlanning/models/Users.models.dart';
import 'package:flutter/foundation.dart';

class LoadProjectsAction {}

class RefreshAction{}

class SetUser {
  final UserModel user;

  SetUser({@required this.user});
}

class SetMonth {
  final int month;

  SetMonth({@required this.month});
}

class SetFilters {
  final UserModel user;
  final int month;

  SetFilters({@required this.user, @required this.month});
}