import 'package:MyPlanning/store/middlewares/projects.middlewares.dart';
import 'package:MyPlanning/store/middlewares/users.middlewares.dart';
import 'package:MyPlanning/store/reducers/reducer.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/state/filters.state.dart';
import 'package:MyPlanning/store/state/projects.state.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';

import 'middlewares/auth.middlewares.dart';

Future<Store<AppState>> createStore() async {

  final persistor = Persistor<AppState>(
    storage: FlutterStorage(location: FlutterSaveLocation.documentFile),
    serializer: JsonSerializer<AppState>(AppState.fromJson),
    //debug: true,
    //throttleDuration: Duration(seconds: 5),
  );

  // Load initial state
  final initialState = await persistor.load();

  var midllewares = projectsMiddleware()
      ..addAll(usersMiddleware())
      ..addAll(authMiddleware())
      ..addAll([new LoggingMiddleware.printer()])
      ..addAll([persistor.createMiddleware()]);


  return Store(
    rootReducer,
      initialState: initialState ?? AppState.initialState(
      projectsState: ProjectsState.initialState(),
      filtersState: FiltersState.initialState()
    ),
    middleware: midllewares
  );

}