import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/store/actions/user.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/state/users.state.dart';
import 'package:redux/redux.dart';

class UsersViewModel {

  final UsersState state;
  final Function(UserModel) onDelete;
  final Function(UserModel) onView;
  final Function(UserModel) onEdit;
  final Function(UserModel) onNew;

  UsersViewModel({this.state, this.onDelete, this.onView, this.onEdit, this.onNew});

  static UsersViewModel fromStore( Store<AppState> store){
    return new UsersViewModel(
      state: store.state.usersState,
      onDelete: (user) => store.dispatch(new DeleteUserAction(user: user)),
      onView: (user) => store.dispatch(new ViewUserAction(user: user)),
      onEdit: (user) => store.dispatch(new EditUserAction(user: user)),
      onNew: (user) => store.dispatch(new NewUserAction(user: user))
    );
  }

  // method to check if the state changed
  @override
  int get hashCode => state.uuid.hashCode;

  bool operator ==(other) {
    bool result = identical(this, other) || other is UsersViewModel;

    if (result) {
      if (state.uuid != (other as UsersViewModel).state.uuid) {
        return false;
      }
    }

    return result;
  }
}