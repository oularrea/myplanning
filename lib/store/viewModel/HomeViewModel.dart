import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/store/actions/filters.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/state/filters.state.dart';
import 'package:MyPlanning/store/state/users.state.dart';
import 'package:redux/redux.dart';

class HomeViewModel {

  final UsersState usersState;
  final FiltersState filtersState;
  final UserModel userAuthModel;
  final Function(UserModel) setUser;
  final Function(int) setMonth;
  final Function(UserModel, int) setFilters;

  HomeViewModel({this.usersState, this.filtersState, this.setUser, this.setMonth, this.setFilters, this.userAuthModel});

  static HomeViewModel fromStore( Store<AppState> store){
    return new HomeViewModel(
      usersState: store.state.usersState,
      filtersState: store.state.filtersState,
      setUser: (user) => store.dispatch(new SetUser(user: user)),
      setMonth: (month) => store.dispatch(new SetMonth(month: month)),
      setFilters: (user, month) => store.dispatch(new SetFilters(user: user ,month: month)),
      userAuthModel: store.state.authState.user
    );
  }

  // method to check if the state changed
  @override
  int get hashCode => filtersState.uuid.hashCode;

  bool operator ==(other) {
    bool result = (identical(this, other) || other is UsersState) || (identical(this, other) || other is FiltersState);

    if (result) {
      if ((usersState.uuid != (other as UsersState).uuid) || (filtersState.uuid != (other as FiltersState).uuid)) {
        return false;
      }
    }

    return result;
  }
}