import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/store/actions/filters.action.dart';
import 'package:MyPlanning/store/actions/user.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/state/filters.state.dart';
import 'package:MyPlanning/store/state/projects.state.dart';
import 'package:MyPlanning/store/state/users.state.dart';
import 'package:redux/redux.dart';

class PlannigViewModel {
  final UsersState users;
  final ProjectsState projects;
  final FiltersState filtersState;
  final UserModel currentUser;
  final Function(UserModel) onUpdate;
  final Function() refreshList;
  final Function(ScheduleModel, UserModel) removeSchedule;

  PlannigViewModel({this.users, this.currentUser, this.filtersState, this.projects, this.onUpdate, this.refreshList, this.removeSchedule});

  static PlannigViewModel fromStore(Store<AppState> store) {
    return new PlannigViewModel(
        users: store.state.usersState,
        projects: store.state.projectsState,
        currentUser: store.state.filtersState.user,
        filtersState: store.state.filtersState,
        onUpdate: (user) => store.dispatch(new UpdateUserAction(user: user)),
        removeSchedule: (schedule, user) => store.dispatch(new RemoveScheduleAction(schedule: schedule, user: user)),
        refreshList: () => store.dispatch(new RefreshAction()));

  }

  // method to check if the state changed
  @override
  int get hashCode => filtersState.uuid.hashCode;

  bool operator ==(other) {
    bool result = identical(this, other) || other is PlannigViewModel;

    if (result) {
      if (filtersState.uuid != (other as PlannigViewModel).filtersState.uuid) {
        return false;
      }
    }

    return result;
  }
}
