import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/store/actions/project.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/state/projects.state.dart';
import 'package:redux/redux.dart';

class ProjectsViewModel {

  final ProjectsState state;
  final Function(ProjectModel) onDelete;
  final Function(ProjectModel) onView;
  final Function(ProjectModel) onEdit;
  final Function(ProjectModel) onNew;

  ProjectsViewModel({this.state, this.onDelete, this.onView, this.onEdit, this.onNew});

  static ProjectsViewModel fromStore( Store<AppState> store){
    return new ProjectsViewModel(
      state: store.state.projectsState,
      onDelete: (project) => store.dispatch(new DeleteProjectAction(project: project)),
      onView: (project) => store.dispatch(new ViewProjectAction(project: project)),
      onEdit: (project) => store.dispatch(new EditProjectAction(project: project)),
      onNew: (project) => store.dispatch(new NewProjectAction(project: project))
    );
  }

  // method to check if the state changed
  @override
  int get hashCode => state.uuid.hashCode;

  bool operator ==(other) {
    bool result = identical(this, other) || other is ProjectsViewModel;

    if (result) {
      if (state.uuid != (other as ProjectsViewModel).state.uuid) {
        return false;
      }
    }

    return result;
  }
}