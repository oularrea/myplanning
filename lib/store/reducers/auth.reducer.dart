import 'package:MyPlanning/store/actions/auth.action.dart';
import 'package:MyPlanning/store/state/auth.state.dart';
import 'package:redux/redux.dart';
import 'package:uuid/uuid.dart';

String uuid() => new Uuid().v1();

AuthState setUser(
    AuthState state, SetUserProfile action) {
  return state.copyWith(u: action.user, uuid: uuid());
}

AuthState loginSuccess(
    AuthState state, LoginSuccesAction action) {
  return state.copyWith(l: true, uuid: uuid(), lo:false);
}

AuthState loginError(
    AuthState state, LoginErrorAction action) {
  return state.copyWith(l: false, uuid: uuid(), lo:false);
}

AuthState logout(
    AuthState state, LogoutAction action) {
  return state.copyWith(u:null ,l:false, uuid: uuid(), lo: false);
}

AuthState login(
    AuthState state, LoginAction action) {
  return state.copyWith(l: true, uuid: uuid());
}

AuthState toggleLoading(AuthState state, ToggleLoadingAction action){
  return state.copyWith(uuid: uuid(), lo:true);
}

final Reducer<AuthState> authReducer = combineReducers([
  new TypedReducer<AuthState, SetUserProfile>(setUser),
  new TypedReducer<AuthState, LoginAction>(login),
  new TypedReducer<AuthState, LoginSuccesAction>(loginSuccess),
  new TypedReducer<AuthState, LoginErrorAction>(loginError),
  new TypedReducer<AuthState, LogoutAction>(logout),
  new TypedReducer<AuthState, ToggleLoadingAction>(toggleLoading),
]);
