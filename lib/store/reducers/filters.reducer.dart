import 'package:MyPlanning/store/actions/filters.action.dart';
import 'package:MyPlanning/store/state/filters.state.dart';
import 'package:redux/redux.dart';
import 'package:uuid/uuid.dart';

String uuid() => new Uuid().v1();

FiltersState setUser(
    FiltersState state, SetUser action) {
  return state.copyWith(u: action.user, uuid: uuid());
}

FiltersState setMonth(
    FiltersState state, SetMonth action) {
  return state.copyWith(m: action.month, uuid: uuid());
}

FiltersState setFilters(
    FiltersState state, SetFilters action) {
  return state.copyWith(u:action.user ,m: action.month, uuid: uuid());
}

FiltersState refreshList(FiltersState state, RefreshAction action){
  return state.copyWith(uuid: uuid());
}

final Reducer<FiltersState> filtersReducer = combineReducers([
  new TypedReducer<FiltersState, SetUser>(setUser),
  new TypedReducer<FiltersState, SetMonth>(setMonth),
  new TypedReducer<FiltersState, SetFilters>(setFilters),
  new TypedReducer<FiltersState, RefreshAction>(refreshList),
]);
