import 'package:MyPlanning/store/reducers/auth.reducer.dart';
import 'package:MyPlanning/store/reducers/filters.reducer.dart';
import 'package:MyPlanning/store/reducers/projects.reducer.dart';
import 'package:MyPlanning/store/reducers/users.reducer.dart';
import 'package:MyPlanning/store/state/app.state.dart';

AppState rootReducer(AppState state, action) {
  return AppState(
      projectsState: projectsReducer(state.projectsState, action),
      filtersState: filtersReducer(state.filtersState, action),
      authState: authReducer(state.authState, action),
      usersState: usersReducer(state.usersState, action))
  ;
}
