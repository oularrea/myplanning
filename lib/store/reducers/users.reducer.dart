import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/store/actions/user.action.dart';
import 'package:MyPlanning/store/state/users.state.dart';
import 'package:redux/redux.dart';
import 'package:uuid/uuid.dart';

String uuid() => new Uuid().v1();

UsersState loadUsers(
    UsersState state, LoadUsersSuccessAction action) {
  return state.copyWith(u: action.users, uuid: uuid());
}

UsersState viewUser(UsersState state, ViewUserAction action) {
  return state.copyWith(s: action.user, uuid: uuid());
}

UsersState editUser(UsersState state, EditUserAction action) {
  return state.copyWith(s: action.user, uuid: uuid());
}

UsersState newUser(UsersState state, NewUserAction action) {
  return state.copyWith(s: action.user, uuid: uuid());
}

UsersState deleteUser(UsersState state, DeleteUserAction action) {
  return state.copyWith(
      u: state.users.where((p) => p.id != action.user.id).toList(),
      uuid: uuid());
}

UsersState saveUser(UsersState state, SaveUserAction action) {
  return state.copyWith(
      uuid: uuid());
}

UsersState updateUser(UsersState state, UpdateUserAction action) {
  List<UserModel> list = state.users;
  return state.copyWith(
      u: list,
      uuid: uuid());
}

UsersState removeSchedule(UsersState state, RemoveScheduleAction action) {
  List<UserModel> list = state.users;
  UserModel userModel = action.user;
  ScheduleModel scheduleModel = action.schedule;
  userModel.schedules.removeWhere((element) => (element.id == scheduleModel.id));

  int index = list.indexWhere((element) => (element.id == userModel.id));
  list[index] = userModel;

  return state.copyWith(
      u: list,
      uuid: uuid());
}

final Reducer<UsersState> usersReducer = combineReducers([
  new TypedReducer<UsersState, LoadUsersSuccessAction>(loadUsers),
  new TypedReducer<UsersState, DeleteUserAction>(deleteUser),
  new TypedReducer<UsersState, ViewUserAction>(viewUser),
  new TypedReducer<UsersState, EditUserAction>(editUser),
  new TypedReducer<UsersState, NewUserAction>(newUser),
  new TypedReducer<UsersState, SaveUserAction>(saveUser),
  new TypedReducer<UsersState, UpdateUserAction>(updateUser),
]);
