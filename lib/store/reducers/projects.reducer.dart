import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/store/actions/project.action.dart';
import 'package:MyPlanning/store/state/projects.state.dart';
import 'package:redux/redux.dart';
import 'package:uuid/uuid.dart';

String uuid() => new Uuid().v1();

ProjectsState loadProjects(
    ProjectsState state, LoadProjectsSuccessAction action) {
  return state.copyWith(p: action.projects, uuid: uuid());
}

ProjectsState viewProject(ProjectsState state, ViewProjectAction action) {
  return state.copyWith(s: action.project, uuid: uuid());
}

ProjectsState editProject(ProjectsState state, EditProjectAction action) {
  return state.copyWith(s: action.project, uuid: uuid());
}

ProjectsState newProject(ProjectsState state, NewProjectAction action) {
  return state.copyWith(s: action.project, uuid: uuid());
}

ProjectsState deleteProject(ProjectsState state, DeleteProjectAction action) {
  return state.copyWith(
      p: state.projects.where((p) => p.id != action.project.id).toList(),
      uuid: uuid());
}

ProjectsState saveProject(ProjectsState state, SaveProjectAction action) {
  //List<ProjectModel> list = state.projects;
  //list.add(action.project);
  return state.copyWith(
      uuid: uuid());
}

ProjectsState updateProject(ProjectsState state, UpdateProjectAction action) {
  List<ProjectModel> list = state.projects;
  return state.copyWith(
      p: list,
      uuid: uuid());
}

final Reducer<ProjectsState> projectsReducer = combineReducers([
  new TypedReducer<ProjectsState, LoadProjectsSuccessAction>(loadProjects),
  new TypedReducer<ProjectsState, DeleteProjectAction>(deleteProject),
  new TypedReducer<ProjectsState, ViewProjectAction>(viewProject),
  new TypedReducer<ProjectsState, EditProjectAction>(editProject),
  new TypedReducer<ProjectsState, NewProjectAction>(newProject),
  new TypedReducer<ProjectsState, SaveProjectAction>(saveProject),
  new TypedReducer<ProjectsState, UpdateProjectAction>(updateProject),
]);
