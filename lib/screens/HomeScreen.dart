import 'dart:ui';
import 'package:MyPlanning/component/CircleButton.dart';
import 'package:MyPlanning/modals/modals.dart';
import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/screens/planning/AddPlanning.dart';
import 'package:MyPlanning/screens/projects/FormProject.dart';
import 'package:MyPlanning/screens/projects/ProjectScreen.dart';
import 'package:MyPlanning/screens/users/UsersScreen.dart';
import 'package:MyPlanning/store/actions/auth.action.dart';
import 'package:MyPlanning/store/actions/filters.action.dart';
import 'package:MyPlanning/store/actions/project.action.dart';
import 'package:MyPlanning/store/actions/user.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/viewModel/HomeViewModel.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:MyPlanning/screens/planning/PlanningScreen.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';
import 'package:date_util/date_util.dart';
import 'package:transparent_image/transparent_image.dart';

const double _fabDimension = 56.0;

class MyHomePage extends StatefulWidget {
  final Store<AppState> store;

  MyHomePage({Key key, this.store}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var dateUtility = new DateUtil();
  int currentIndex;
  PageController _pageController;
  final tabs = [Planning(), Users(), Projects()];

  List<UserModel> users = [];

  ContainerTransitionType _transitionType = ContainerTransitionType.fade;

  // Month
  int monthSelected;
  int selectUser;

  @override
  void initState() {
    super.initState();

    currentIndex = 0;
    _pageController = PageController();
    monthSelected = widget.store.state.filtersState.month;
    selectUser = 0;
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void changePage(int index) {
    setState(() {
      currentIndex = index;
    });
    _pageController.jumpToPage(index);
    _pageController.animateToPage(index,
        duration: Duration(milliseconds: 500), curve: Curves.easeInOutQuad);
  }

  // Month controller
  void onGestureDownMonth(Function(int) setMonth) {
    int toChange = monthSelected == 1 ? 12 : monthSelected - 1;
    setState(() {
      monthSelected = toChange;
    });
    setMonth(toChange);
  }

  void onGestureUpMonth(Function(int) setMonth) {
    int toChange = monthSelected == 12 ? 1 : monthSelected + 1;
    setState(() {
      monthSelected = toChange;
    });
    setMonth(toChange);
  }

  void onSelectedMonth(int value, Function(int) setMonth) {
    setState(() {
      monthSelected = value;
    });
    setMonth(value);
  }

  void onTabMonthTab(
      BuildContext context, int currentSelected, Function(int) setMonth) async {
    return showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  topRight: Radius.circular(20.0)),
              child: Container(
                  height: 300,
                  child: ListView(
                      children: [
                    ListTile(
                        leading: Material(
                          color: Colors.transparent,
                          child: InkWell(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: Icon(Icons.close) // the arrow back icon
                              ),
                        ),
                        title: Center(
                            child:
                                Text("Seleccione el mes") // Your desired title
                            )),
                  ]..addAll(renderOptionsMonth(
                          context, currentSelected, setMonth)))));
        });
  }

  DateTime nextMonth(DateTime current, int addDays) {
    return current.add(new Duration(days: addDays));
  }

  List<Widget> renderOptionsMonth(
      BuildContext context, int currentSelected, Function(int) setMonth) {
    var currentDate = new DateTime.now();
    var beginDate = new DateTime(currentDate.year, 1, 1);
    return [
      Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              renderElement(context, currentSelected, beginDate, 1, setMonth),
              renderElement(context, currentSelected, beginDate, 2, setMonth),
              renderElement(context, currentSelected, beginDate, 3, setMonth)
            ],
          )
        ],
      ),
      Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              renderElement(context, currentSelected, beginDate, 4, setMonth),
              renderElement(context, currentSelected, beginDate, 5, setMonth),
              renderElement(context, currentSelected, beginDate, 6, setMonth)
            ],
          )
        ],
      ),
      Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              renderElement(context, currentSelected, beginDate, 7, setMonth),
              renderElement(context, currentSelected, beginDate, 8, setMonth),
              renderElement(context, currentSelected, beginDate, 9, setMonth)
            ],
          )
        ],
      ),
      Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              renderElement(context, currentSelected, beginDate, 10, setMonth),
              renderElement(context, currentSelected, beginDate, 11, setMonth),
              renderElement(context, currentSelected, beginDate, 12, setMonth)
            ],
          )
        ],
      )
    ];
  }

  // Team controller
  void onGestureDownUser(Function(UserModel) setUser) {
    int toChange = selectUser == 0 ? this.users.length - 1 : selectUser - 1;
    setState(() {
      selectUser = toChange;
    });
    setUser(users[toChange]);
  }

  void onGestureUpUser(Function(UserModel) setUser) {
    int toChange = selectUser == this.users.length ? 0 : selectUser + 1;
    setState(() {
      selectUser = toChange;
    });
    setUser(users[toChange]);
  }

  void onSelectedUser(UserModel user) {
    var index = users.indexOf(user);
    setState(() {
      selectUser = index;
    });
    widget.store.dispatch(new SetUser(user: users[index]));
  }

  void onTabUserTab(BuildContext context) async {
    await AnimatedDialogBox.showScaleAlertBox(
        title: Column(
          children: <Widget>[
            Center(child: Text("Miembros del equipo")),
            Divider()
          ],
        ),
        context: context,
        yourWidget: Container(
          width: MediaQuery.of(context).size.width - 50,
          height: (MediaQuery.of(context).size.height / 2) - 100,
          child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 2.0),
              children: _renderOptionsUser(context)),
        ));
  }

  List<Widget> _renderOptionsUser(BuildContext context) {
    return this.users.map((UserModel user) {
      return ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(user.photoUrl),
        ),
        title: Text(user.displayName),
        subtitle: Text(user.email),
        onTap: () {
          onSelectedUser(user);
          Navigator.of(context).pop();
        },
        trailing: user.id == users[selectUser].id
            ? Icon(
                Icons.check_circle,
                color: Theme.of(context).primaryColor,
              )
            : null,
      );
    }).toList();
  }

  Widget renderElement(BuildContext context, int currentSelected,
      DateTime current, int month, Function(int) setMonth) {
    return Center(
      child: InkResponse(
        onTap: () {
          print("selected $month");
          onSelectedMonth(month, setMonth);
          Navigator.of(context).pop();
        },
        child: Chip(
          backgroundColor: currentSelected == month ? Colors.grey : null,
          label: Text(
              DateFormat("MM/yy").format(nextMonth(current, (month - 1) * 31))),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    UserModel userSelected;

    return StoreConnector<AppState, HomeViewModel>(
        distinct: true,
        rebuildOnChange: true,
        onInit: (store) => store.dispatch(new LoadUsersAction()),
        converter: (store) => HomeViewModel.fromStore(store),
        onDidChange: (vm) {
          this.users = vm.usersState.users;
          this.monthSelected = vm.filtersState.month;
          userSelected = vm.filtersState.user ?? vm.userAuthModel;
          if (userSelected != null)
            selectUser =
                this.users.indexWhere((user) => user.id == userSelected.id, 0);
          else
            selectUser = 0;
        },
        builder: (_, vm) {
          userSelected = vm.filtersState.user ?? vm.userAuthModel;

          return Scaffold(
            appBar: AppBar(
              title: Center(child: Text('My Planning')),
              actions: <Widget>[
                currentIndex == 2
                    ? CircleButton(
                        onTap: () =>
                            onTabMonthTab(context, monthSelected, vm.setMonth),
                        content: Row(
                          children: <Widget>[
                            Icon(Icons.calendar_today),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              child: Container(
                                child: Text(dateUtility.month(monthSelected),
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                    )),
                              ),
                            ),
                          ],
                        ),
                        onPointerDow: () => onGestureDownMonth(vm.setMonth),
                        onPointerUp: () => onGestureUpMonth(vm.setMonth),
                      )
                    : Container(),
                (userSelected != null && currentIndex == 0)
                    ? new CircleButton(
                        onTap: () => onTabUserTab(context),
                        content: AnimatedContainer(
                            duration: Duration(milliseconds: 4000),
                            curve: Curves.fastOutSlowIn,
                            child: Material(
                                elevation: 4.0,
                                shape: CircleBorder(),
                                clipBehavior: Clip.hardEdge,
                                child: FadeInImage.memoryNetwork(
                                  placeholder: kTransparentImage,
                                  image: userSelected.photoUrl,
                                  fit: BoxFit.cover,
                                  width: 40.0,
                                  height: 40.0
                                ))),
                        onPointerDow: () => onGestureDownUser(vm.setUser),
                        onPointerUp: () => onGestureUpUser(vm.setUser),
                      )
                    : CircularProgressIndicator(),
              ],
            ),
            floatingActionButton: OpenContainer(
              transitionType: _transitionType,
              openBuilder: (BuildContext context, VoidCallback _) {
                if (currentIndex == 2) {
                  return FormProject();
                }

                if (currentIndex == 1) {
                  //return widget.store.dispatch(new LogoutAction());
                }

                if (currentIndex == 0) {
                  return AddPlanning();
                }

                return FormProject();
              },
              closedElevation: 6.0,
              closedShape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(_fabDimension / 2),
                ),
              ),
              closedColor: Theme.of(context).colorScheme.secondary,
              closedBuilder:
                  (BuildContext context, VoidCallback openContainer) {
                return SizedBox(
                  height: _fabDimension,
                  width: _fabDimension,
                  child: InkWell(
                    onTap: () {
                      /*if (currentIndex == 0) {
                        Navigator.of(context).pushNamed('/form-planning');
                      }
                      if (currentIndex == 1) {
                        Navigator.of(context).pushNamed('/form-team');
                      }*/
                      if (currentIndex == 2) {
                        widget.store.dispatch(new NewProjectAction(
                            project: new ProjectModel(id: "new")));
                      }

                      openContainer();
                    },
                    child: Center(
                      child: Icon(
                        Icons.add,
                        color: Theme.of(context).colorScheme.onSecondary,
                      ),
                    ),
                  ),
                );
              },
            ),
            /*floatingActionButton: FloatingActionButton(
                onPressed: () {
                  if (currentIndex == 0) {
                    Navigator.of(context).pushNamed('/form-planning');
                  }
                  if (currentIndex == 1) {
                    Navigator.of(context).pushNamed('/form-team');
                  }
                  if (currentIndex == 2) {
                    widget.store.dispatch(new NewProjectAction(
                        project: new ProjectModel(id: "new")));
                  }
                },
                child: Icon(Icons.add)),*/
            floatingActionButtonLocation:
                FloatingActionButtonLocation.endDocked,
            bottomNavigationBar: BubbleBottomBar(
              opacity: .2,
              currentIndex: currentIndex,
              onTap: changePage,
              borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
              elevation: 8,
              fabLocation: BubbleBottomBarFabLocation.end,
              //new
              hasNotch: true,
              //new
              hasInk: true,
              //new, gives a cute ink effect
              inkColor: Colors.black12,
              //optional, uses theme color if not specified
              items: <BubbleBottomBarItem>[
                BubbleBottomBarItem(
                    backgroundColor: Colors.red,
                    icon: Icon(
                      Icons.access_time,
                      color: Colors.black,
                    ),
                    activeIcon: Icon(
                      Icons.access_time,
                      color: Colors.red,
                    ),
                    title: Text("Planning")),
                BubbleBottomBarItem(
                    backgroundColor: Colors.deepPurple,
                    icon: Icon(
                      Icons.group,
                      color: Colors.black,
                    ),
                    activeIcon: Icon(
                      Icons.group,
                      color: Colors.deepPurple,
                    ),
                    title: Text("Team")),
                BubbleBottomBarItem(
                    backgroundColor: Colors.indigo,
                    icon: Icon(
                      Icons.folder_open,
                      color: Colors.black,
                    ),
                    activeIcon: Icon(
                      Icons.folder_open,
                      color: Colors.indigo,
                    ),
                    title: Text("Projects"))
              ],
            ),
            body: SizedBox.expand(
              child: PageView(
                  controller: _pageController,
                  onPageChanged: (index) {
                    setState(() => currentIndex = index);
                  },
                  children: this.tabs // tabs
                  ),
            ),
          );
        });
  }
}
