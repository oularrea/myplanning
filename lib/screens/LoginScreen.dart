import 'package:MyPlanning/component/LoadingAnimation.dart';
import 'package:MyPlanning/providers/auth.services.dart';
import 'package:MyPlanning/store/actions/auth.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:flutter/material.dart';
import 'package:MyPlanning/component/AvatarGlow.dart';
import 'package:MyPlanning/component/PowerBy.dart';
import 'package:MyPlanning/component/delayedAnimation.dart';
import 'package:flutter_redux/flutter_redux.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> with SingleTickerProviderStateMixin {
  final int delayedAmount = 200;
  double _scale;
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
      lowerBound: 0.0,
      upperBound: 0.1,
    )..addListener(() {
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final color = Colors.white;
    _scale = 1 - _controller.value;
    return StoreConnector<AppState, bool>(
      distinct: true,
      rebuildOnChange: true,
      converter: (store){
        return store.state.authState.loading != null?store.state.authState.loading: false;
      },
      builder: (_, bool loading) {
        return Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Theme.of(context).primaryColor,
            body: Center(
              child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                      Theme.of(context).primaryColor,
                      Theme.of(context).primaryColorDark
                    ])),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 30),
                      child: AnimateLogo(),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: <Widget>[
                          DelayedAnimation(
                            child: Text(
                              "Bienvenido",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 35.0,
                                  color: color),
                            ),
                            delay: delayedAmount + 400,
                          ),
                          DelayedAnimation(
                            child: Text(
                              "a",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 35.0,
                                  color: color),
                            ),
                            delay: delayedAmount + 500,
                          ),
                          DelayedAnimation(
                            child: Text(
                              "My Planning",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 35.0,
                                  color: color),
                            ),
                            delay: delayedAmount + 600,
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(),
                    ),
                    loading
                        ? Loading()
                        : DelayedAnimation(
                            child: GestureDetector(
                              onTapDown: _onTapDown,
                              onTapUp: _onTapUp,
                              child: Transform.scale(
                                scale: _scale,
                                child: this._animatedButtonUI(context),
                              ),
                            ),
                            delay: delayedAmount + 700,
                          ),
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                    DelayedAnimation(
                      child: PowerBy(),
                      delay: delayedAmount + 800,
                    ),
                  ],
                ),
              ),
            ));
      },
    );
  }

  Widget _animatedButtonUI(BuildContext context) => Container(
        height: 50,
        width: 280,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Colors.white,
        ),
        child: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/google-g-logo-png.png',
              height: 42,
              width: 42,
            ),
            SizedBox(
              width: 5.0,
            ),
            Text(
              'Inicia sesión con Google',
              style: TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ],
        )),
      );

  void _onTapDown(TapDownDetails details) {
    _controller.forward();
    _onLogin();
  }

  void _onTapUp(TapUpDetails details) {
    _controller.reverse();
  }

  void _onLogin() {
    var store = StoreProvider.of<AppState>(context);
    store.dispatch(new LoginAction());
  }
}
