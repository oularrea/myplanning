import 'package:MyPlanning/component/UserItemList.dart';
import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/store/actions/user.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/viewModel/UsersViewModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class Users extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UsersViewModel>(
        distinct: true,
        rebuildOnChange: true,
        onInit: (store) => store.dispatch(new LoadUsersAction()),
        converter: (store) => UsersViewModel.fromStore(store),
        builder: (_, vm) {
          return ListView.builder(
              itemCount: vm.state.users.length,
              itemBuilder: (_, int index) {
                UserModel item = vm.state.users[index];
                return UsersItem(userModel: item, onView: vm.onView,);
              });
        });
  }
}
