import 'dart:async';

import 'package:MyPlanning/component/LoadingAnimation.dart';
import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/screens/planning/AddPlanning.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/viewModel/PlannigViewModel.dart';
import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:side_header_list_view/side_header_list_view.dart';
import 'package:transparent_image/transparent_image.dart';

class Planning extends StatefulWidget {
  @override
  _PlanningState createState() => _PlanningState();
}

class _PlanningState extends State<Planning> {
  List<ScheduleModel> schedules;
  final ContainerTransitionType _transitionType = ContainerTransitionType.fade;
  int toScroll = 0;
  var currentDate = DateTime.now();

  Future time(int time) async {
    Completer c = new Completer();
    new Timer(new Duration(seconds: time), () {
      setState(() {});
      c.complete('done with time out');
    });

    return c.future;
  }

  @override
  void initState() {
    super.initState();
    time(1);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PlannigViewModel>(
        distinct: true,
        rebuildOnChange: true,
        onInit: (initState) {
          int indexUser = initState.state.usersState.users.indexWhere(
              (element) => element.id == initState.state.filtersState.user.id,
              0);
          UserModel currentUser = initState.state.usersState.users[indexUser];
          schedules = currentUser?.generateSchedule();
          if (schedules == null) schedules = [];
          toScroll = schedules.indexWhere((element) =>
              element.createdAt.difference(currentDate).inDays == 0);
        },
        onDidChange: (vm) {
          int indexUser = vm.users.users
              .indexWhere((element) => element.id == vm.currentUser.id, 0);
          UserModel currentUser = vm.users.users[indexUser];
          schedules = currentUser.generateSchedule();
          toScroll = schedules.indexWhere((element) =>
              element.createdAt.difference(currentDate).inDays == 0);
          setState(() {

          });
        },
        converter: (store) => PlannigViewModel.fromStore(store),
        builder: (_, vm) {
          if (vm.currentUser == null) return Loading();

          if (schedules == null) return Loading();

          return Stack(
            children: <Widget>[
              Container(
                child: SideHeaderListView(
                  scrollToPosition: toScroll,
                  itemCount: schedules.length,
                  itemExtend: 80.0,
                  headerBuilder: (BuildContext context, int index) {
                    return _renderHeader(index);
                  },
                  itemBuilder: (BuildContext context, int index) {
                    return _renderItem(index, vm.currentUser, vm.removeSchedule);
                  },
                  hasSameHeader: (int a, int b) {
                    return schedules[a].createdAt.day == schedules[b].createdAt.day;
                  },
                ),
              ),
              Positioned(
                bottom: 45,
                right: 15,
                child: FloatingActionButton(onPressed: (){setState(() {

                });}, child: Icon(Icons.arrow_drop_up), tooltip: "Hoy",),
              )
            ],
          );
        });
  }

  Widget _renderWeekDay(DateTime currentDate, ScheduleModel beforeElement) {

    if(beforeElement?.createdAt?.day == currentDate.day){
      return Container();
    }

    if (currentDate.weekday == DateTime.monday) {
      var beginWeek = currentDate;
      var endWeek = currentDate.add(new Duration(days: 6));
      var text = "";
      if (beginWeek.month != endWeek.month) {
        text = DateFormat("d MMM.").format(beginWeek);
      } else {
        text = DateFormat("d").format(beginWeek);
      }
      text += " - ";
      text += DateFormat("d MMM.").format(endWeek);

      return Container(
        child: Align(
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 15, top: 2, bottom: 2),
            child: Text(
              text,
              style: TextStyle(fontSize: 12),
            ),
          ),
        ),
      );
    }
    return Container();
  }
  
  Widget _renderItem(int index,UserModel user, Function(ScheduleModel, UserModel) removeSchedule){
    ScheduleModel item = schedules[index];
    ScheduleModel before = index -1 < 0?null:schedules[index -1];

    //Separate Between Month
    if (item.isFirstMonth) {
      return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
          ),
          height: double.infinity,
          width: double.infinity,
          child: Stack(fit: StackFit.expand, children: <Widget>[
            FadeInImage.memoryNetwork(
              image: "https://picsum.photos/400/300?random=" +
                  index.toString() +
                  ".webp",
              fit: BoxFit.cover, placeholder: kTransparentImage,
            ),
            Positioned(
              left: 5,
              top: 5,
              child: Text(
                  DateFormat("yMMMM").format(
                      item.createdAt.add(new Duration(days: 1))),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                      color: Colors.white)),
            )
          ]));
    }

    //Without Projects and Current Date
    if (_isSameDate(item.createdAt , currentDate) &&
        item.projectModel == null) {
      return OpenContainer(
        transitionType: _transitionType,
        transitionDuration: new Duration(milliseconds: 300),
        tappable: false,
        closedShape: const RoundedRectangleBorder(),
        closedElevation: 0.0,
        openBuilder: (BuildContext _, VoidCallback openContainer) {
          return AddPlanning(dateTime: item.createdAt,);
        },
        closedBuilder: (BuildContext _, VoidCallback openContainer){
          return Container(
            child: Column(
              children: <Widget>[
                _renderWeekDay(item.createdAt, before),
                AnimatedContainer(
                  width: double.infinity,
                  height: 50,
                  duration: Duration(seconds: 2),
                  curve: Curves.easeIn,
                  padding: EdgeInsets.only(
                      bottom: 2, top: 8.0, right: 8.0, left: 8.0),
                  child: Material(
                    borderRadius: BorderRadius.circular(5.0),
                    color: Theme.of(context).primaryColor,
                    child: InkWell(
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.add_circle),
                            SizedBox(width: 5),
                            Text(
                              "Planificar",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        return openContainer();
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 2,
                ),
                Container(
                  height: 10,
                  child: Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          width: 10,
                          height: 10,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              shape: BoxShape.circle),
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          width: double.infinity,
                          height: 1,
                          padding: EdgeInsets.only(
                              right: 10, left: 10, top: 8.0),
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              shape: BoxShape.rectangle),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        },
      );
    }

    //Without Projects
    if (item.projectModel == null) {
      return OpenContainer(
        transitionType: _transitionType,
        transitionDuration: new Duration(milliseconds: 300),
        tappable: false,
        closedShape: const RoundedRectangleBorder(),
        closedElevation: 0.0,
        openBuilder: (BuildContext _, VoidCallback openContainer) {
          return AddPlanning(dateTime: item.createdAt,);
        },
        closedBuilder: (_, VoidCallback openContainer){
          return Column(
            children: <Widget>[
              _renderWeekDay(item.createdAt, before),
              AnimatedContainer(
                width: double.infinity,
                height: 50,
                duration: Duration(seconds: 2),
                curve: Curves.easeIn,
                padding: EdgeInsets.all(8.0),
                child: Material(
                  borderRadius: BorderRadius.circular(5.0),
                  color: Theme.of(context).buttonColor,
                  child: InkWell(
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.add_circle),
                          SizedBox(width: 5),
                          Text(
                            "Planificar",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      return openContainer();
                    },
                  ),
                ),
              ),
            ],
          );
        },
      );
    }

    //With Projects
    return Dismissible(
      key: ObjectKey(item),
      onDismissed: (direction) {
        removeSchedule(item, user);
        setState(() {
          schedules.remove(item);
        });
      },
      child: Column(
        children: <Widget>[
          _renderWeekDay(item.createdAt, before),
          AnimatedContainer(
            width: double.infinity,
            height: 60,
            duration: Duration(seconds: 2),
            curve: Curves.easeIn,
            padding: EdgeInsets.all(8.0),
            child: Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  color: Color(int.parse(item.projectModel.shadeColor,
                      radix: 16) +
                      0xFF000000),
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        item.projectModel.name + " ("+item.hours.toString()+"h)",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w700),
                      ),
                    )
                  ],
                )),
          ),
        ],
      ),
    );
  }

  Widget _renderHeader(int index){
    ScheduleModel item = schedules[index];
    if (item.isFirstMonth) {
      return Container(
        height: 1,
        width: 1,
      );
    }
    if (_isSameDate(item.createdAt, currentDate)) {
      return new SizedBox(
          width: 64.0,
          height: 72.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                DateFormat('EEE').format(item.createdAt),
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                    color: Theme.of(context).primaryColor),
              ),
              SizedBox(
                height: 2,
              ),
              Container(
                width: 35,
                height: 35,
                margin: EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    shape: BoxShape.circle),
                child: Center(
                  child: Text(
                    DateFormat('d').format(item.createdAt),
                    style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 16.0,
                        color: Theme.of(context)
                            .textTheme
                            .headline
                            .color),
                  ),
                ),
              )
            ],
          ));
    }
    return new SizedBox(
        width: 64.0,
        height: 72.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              DateFormat('EEE').format(item.createdAt),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12.0,
              ),
            ),
            Container(
              margin: EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  shape: BoxShape.circle),
              child: Center(
                child: Text(
                  DateFormat('d').format(item.createdAt),
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 16.0,
                  ),
                ),
              ),
            )
          ],
        ));
  }

  bool _isSameDate(DateTime a, DateTime b){
    return (a.day == b.day &&
        a.month == b.month &&
        a.year == b.year);
  }
}
