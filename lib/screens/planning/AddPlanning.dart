import 'package:MyPlanning/component/ContentCard.dart';
import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/models/Users.models.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/viewModel/PlannigViewModel.dart';
import 'package:chips_choice/chips_choice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

String uuid() => new Uuid().v1();

class AddPlanning extends StatefulWidget {
  final DateTime dateTime;

  AddPlanning({this.dateTime});

  @override
  _AddPlanningState createState() => _AddPlanningState();
}

class _AddPlanningState extends State<AddPlanning> {
  List<String> selectedItemsProjects = [];
  List<String> selectedItemsUsers = [];
  final List<Map<String, String>> items = [];
  List<DateTime> picked;
  List<DateTime> dateRange = [];
  int cantHours = 8;

  @override
  void initState() {
    super.initState();
    if (widget.dateTime != null) {
      dateRange.add(widget.dateTime);
      dateRange.add(widget.dateTime);
    } else {
      dateRange.add(DateTime.now());
      dateRange.add(DateTime.now());
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PlannigViewModel>(
      converter: (store) => PlannigViewModel.fromStore(store),
      onInitialBuild: (PlannigViewModel vm) {
        vm.projects.projects.forEach((element) {
          items.add({'label': element.name, 'value': element.id});
        });

        selectedItemsUsers.add(vm.currentUser.id);
      },
      builder: (BuildContext context, PlannigViewModel vm) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Container(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        shape: CircleBorder(),
                        color: Colors.grey[200],
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Icon(Icons.close) // the arrow back icon
                            ),
                      )
                    ],
                  ),
                  Content(
                    title: 'Proyectos',
                    child: FormField<List<String>>(
                      key: Key("projects"),
                      autovalidate: true,
                      initialValue: selectedItemsProjects,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Seleccione al menos un proyecto';
                        }
                        if (value.length > 3) {
                          return "No debe seleccionar más de 3 proyectos a la vez";
                        }
                        return null;
                      },
                      builder: (state) {
                        return Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.centerLeft,
                              child: ChipsChoice<String>.multiple(
                                value: state.value,
                                options: ChipsChoiceOption.listFrom<String,
                                    Map<String, String>>(
                                  source: items,
                                  value: (index, item) => item['value'],
                                  label: (index, item) => item['label'],
                                ),
                                onChanged: (val) {
                                  state.didChange(val);
                                  this.setState(() {
                                    selectedItemsProjects = val;
                                  });
                                },
                                itemConfig: ChipsChoiceItemConfig(
                                  selectedColor: Colors.indigo,
                                  selectedBrightness: Brightness.dark,
                                  unselectedColor: Colors.indigo,
                                  unselectedBorderOpacity: .3,
                                ),
                                isWrapped: true,
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  state.errorText ??
                                      state.value.length.toString() +
                                          '/3 seleccionados',
                                  style: TextStyle(
                                      color: state.hasError
                                          ? Colors.redAccent
                                          : Theme.of(context).primaryColor),
                                ))
                          ],
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Content(
                      title: "Cantidad(hrs)",
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: TextFormField(
                          autovalidate: true,
                          initialValue: cantHours.toString(),
                          textInputAction: TextInputAction.next,
                          validator: (value) => value.isEmpty
                              ? 'Debe tener unas horas asignadas'
                              : null,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              hintText: '(hrs)',
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              suffixIcon: Icon(Icons.access_time)),
                          onChanged: (value) {
                            setState(() {
                              cantHours = int.parse(value);
                            });
                          },
                        ),
                      )),
                  SizedBox(
                    height: 20,
                  ),
                  Content(
                    title: "Fechas",
                    child: ListTile(
                      leading: Icon(Icons.calendar_today),
                      title: dateRange.length > 0
                          ? Row(
                              children: <Widget>[
                                Text(DateFormat.yMd('es_Es')
                                    .format(dateRange[0])),
                                SizedBox(
                                  width: 5,
                                ),
                                Text("-"),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(DateFormat.yMd('es_Es')
                                    .format(dateRange[1])),
                              ],
                            )
                          : Container(),
                      onTap: () async {
                        picked = await DateRagePicker.showDatePicker(
                            context: context,
                            initialFirstDate: dateRange[0],
                            initialLastDate: dateRange[1],
                            firstDate: new DateTime(2019),
                            lastDate: new DateTime(2021));
                        if (picked != null && picked.length == 2) {
                          this.setState(() {
                            dateRange = picked;
                          });
                        }
                        if (picked != null && picked.length == 1) {
                          dateRange = [];
                          dateRange.add(picked[0]);
                          dateRange.add(picked[0]);
                          this.setState(() {});
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Content(
                    title: 'Usuarios',
                    child: FormField<List<String>>(
                      key: Key("users"),
                      autovalidate: true,
                      initialValue: selectedItemsUsers,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Seleccione al menos un usuario';
                        }
                        if (value.length > 5) {
                          return "No debe seleccionar más de 5 usuarios a la vez";
                        }
                        return null;
                      },
                      builder: (state) {
                        return Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.centerLeft,
                              child: ChipsChoice<String>.multiple(
                                value: state.value,
                                options: ChipsChoiceOption.listFrom<String,
                                    UserModel>(
                                  source: vm.users.users,
                                  value: (index, item) => item.id,
                                  label: (index, item) => item.displayName,
                                  avatar: (index, item) => CircleAvatar(
                                    backgroundImage:
                                        NetworkImage(item.photoUrl),
                                  ),
                                ),
                                itemConfig: ChipsChoiceItemConfig(
                                  selectedColor: Theme.of(context).primaryColor,
                                  unselectedColor: Colors.blueGrey,
                                  selectedBrightness: Brightness.dark,
                                  unselectedBrightness: Brightness.dark,
                                  showCheckmark: false,
                                ),
                                isWrapped: true,
                                onChanged: (val) {
                                  state.didChange(val);
                                  this.setState(() {
                                    selectedItemsUsers = val;
                                  });
                                },
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.fromLTRB(15, 0, 15, 15),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  state.errorText ??
                                      state.value.length.toString() +
                                          '/5 seleccionados',
                                  style: TextStyle(
                                      color: state.hasError
                                          ? Colors.redAccent
                                          : Colors.green),
                                )),
                          ],
                        );
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2,
                    child: RaisedButton(
                      onPressed: this.isDisabledButton()
                          ? () => this.onAddSchedule(vm)
                          : null,
                      child: Center(child: Text("Planificar")),
                      color: Theme.of(context).primaryColor,
                    ),
                  )
                ],
              ),
            )),
          ),
        );
      },
    );
  }

  bool isDisabledButton() {
    return (dateRange.length > 0 &&
        selectedItemsUsers.length > 0 &&
        selectedItemsProjects.length > 0);
  }
  
  void onAddSchedule(vm){
    List<UserModel> userSelecteds =
    vm.users.users.where((element) {
      if (selectedItemsUsers.indexOf(element.id) >=
          0) {
        return true;
      }
      return false;
    }).toList();

    List<ProjectModel> projectsSelecteds =
    vm.projects.projects.where((element) {
      if (selectedItemsProjects.indexOf(element.id) >=
          0) {
        return true;
      }
      return false;
    }).toList();

    userSelecteds.forEach((user) {
      projectsSelecteds.forEach((project) {
        DateTime begin = dateRange[0];
        DateTime end = dateRange[1].add(new Duration(days: 1));
        DateTime iteration = begin;

        while (iteration.isBefore(end)) {
          if (iteration.weekday !=
              DateTime.saturday &&
              iteration.weekday != DateTime.sunday) {
            user.schedules.add(new ScheduleModel(
                createdAt: iteration,
                projectModel: project,
                id: uuid(),
                hours: cantHours, isFirstMonth: iteration.day == 1));
          }

          iteration =
              iteration.add(new Duration(days: 1));
        }
      });
      vm.onUpdate(user);
    });

    vm.refreshList();

    Navigator.of(context).pop();
  }
}
