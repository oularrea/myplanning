import 'package:MyPlanning/component/LoadingLottie.dart';
import 'package:MyPlanning/providers/auth.services.dart';
import 'package:MyPlanning/store/actions/user.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:MyPlanning/component/AvatarGlow.dart';
import 'package:MyPlanning/component/PowerBy.dart';
import 'package:flutter_redux/flutter_redux.dart';

checkIfAuthenticated() async {
  AuthServices _firebaseAuth = new AuthServices();
  return _firebaseAuth.checkLogin();
}

class Landing extends StatefulWidget {

  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> with SingleTickerProviderStateMixin {

  @override
  void initState() {
    super.initState();

    checkIfAuthenticated().then((success) {
      if (success) {
        var store = StoreProvider.of<AppState>(context);
        store.dispatch(new LoadUsersAction());
        Navigator.pushReplacementNamed(context, '/home');
      } else {
        Navigator.pushReplacementNamed(context, '/login');
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Theme.of(context).primaryColor, Theme.of(context).primaryColorDark])),
          child: Center(
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 100.0),
                      child: AnimateLogo()),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: LoadingLottie(),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                      padding: const EdgeInsets.all(8.0), child: PowerBy()),
                )
              ],
            ),
          )),
    );
  }
}
