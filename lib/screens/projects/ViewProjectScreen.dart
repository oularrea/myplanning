import 'package:MyPlanning/component/CardStatsProjectView.dart';
import 'package:MyPlanning/component/NextPlanningTeam.dart';
import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/viewModel/ProjectsViewModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';

class ViewProject extends StatefulWidget {
  @override
  _ViewProjectState createState() => _ViewProjectState();
}

class _ViewProjectState extends State<ViewProject> {
  final controller = ScrollController();
  double offset = 0;

  @override
  void initState() {
    super.initState();
    controller.addListener(onScroll);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ProjectsViewModel>(
      converter: (store) => ProjectsViewModel.fromStore(store),
      builder: (context, vm) {
        Color background = Color(
            int.parse(vm.state.selected.shadeColor, radix: 16) + 0xFF000000);

        return Scaffold(
          appBar: AppBar(
            backgroundColor: background ?? null,
            title: Text(vm.state.selected.name),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () => vm.onEdit(vm.state.selected)),
              IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () => vm.onDelete(vm.state.selected))
            ],
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: <Widget>[
                      _renderInfo(vm.state.selected),
                      SizedBox(height: 10),
                      _renderStats(vm.state.selected),
                      SizedBox(height: 20),
                      _renderTitleDivider("Planificado en", (){}),
                      SizedBox(height: 20),
                      _renderNextPlanning(),
                    ],
                  ),
                )
                //_renderPlanningFuture()
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _renderInfo(ProjectModel projectModel) {
    return Container(
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 4),
              blurRadius: 30,
              color: Colors.grey[200],
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            Center(child: Text("Mantenimiento", style: Theme.of(context).textTheme.headline6)),
            SizedBox(height: 10),
            projectModel.maintenance.isMaintenaince?
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Center(child: Text("Desde: "+ DateFormat("yMMM").format(projectModel.maintenance.from), style: Theme.of(context).textTheme.subtitle1)),
                SizedBox(width: 10),
                Center(child: Text("Hasta: "+ DateFormat("yMMM").format(projectModel.maintenance.to), style: Theme.of(context).textTheme.subtitle1)),
              ],
            ):
                Container()
          ],
        ),
    );
  }

  Widget _renderStats(ProjectModel projectModel) {
    int total = 0;
    int currentMonth = 0;
    int planning = 0;
    projectModel.planning.forEach((element) {
      total += element.total;
      if(element.date.month == DateTime.now().month){
        currentMonth += element.total;
      }
    });

    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 4),
            blurRadius: 30,
            color: Colors.grey[200],
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Counter(
            color: Color(
                int.parse(projectModel.shadeColor, radix: 16) + 0xFF000000),
            number: total,
            title: "Total(h)",
          ),
          Counter(
            color: Color(
                int.parse(projectModel.mainColor, radix: 16) + 0xFF000000),
            number: currentMonth,
            title: "Este mes(h)",
          ),
          Counter(
            color: Colors.grey[600],
            number: planning,
            title: "Planificadas(h)",
          ),
        ],
      ),
    );
  }

  Widget _renderTitleDivider(String title, Function() action) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
        Text(
          "Ver detalles",
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }

  Widget _renderNextPlanning() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          NextMemberCard(
            image: "assets/avatars/Abbott.jpg",
            title: "14/05/2020 - 25/05/2020",
          ),
          NextMemberCard(
            image: "assets/avatars/alice.jpg",
            title: "Caugh",
          ),
          NextMemberCard(
            image: "assets/avatars/andrew.jpg",
            title: "Fever",
          ),
        ],
      ),
    );
  }
}
