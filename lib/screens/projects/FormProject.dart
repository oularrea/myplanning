import 'package:MyPlanning/component/LoadingAnimation.dart';
import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/store/actions/project.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

class FormProject extends StatefulWidget {
  @override
  _FormProjectState createState() => _FormProjectState();
}

class _FormProjectState extends State<FormProject> {
  Flushbar flush;
  bool isDisabled = true;

  ProjectModel _projectModel;

  Color _tempMainColor;
  Color _tempShadeColor;
  ColorSwatch _mainColor = Colors.blueGrey;
  Color _shadeColor = Color(int.parse("ff607d8b", radix: 16) + 0xFF000000);

  final _formKey = GlobalKey<FormState>();
  final FocusNode _nameFocus = FocusNode();
  final FocusNode _hourFocus = FocusNode();
  final _fromController = TextEditingController();
  final _toController = TextEditingController();
  final _nameController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Limpia el controlador cuando el widget se elimine del árbol de widgets
    _fromController.dispose();
    _toController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  void _onSubmit() {
    _projectModel.shadeColor =
        this._shadeColor.value.toRadixString(16).padLeft(8, '0');
    _projectModel.mainColor =
        this._shadeColor.value.toRadixString(16).padLeft(8, '0');
    var store = StoreProvider.of<AppState>(context);

    if (_projectModel.id == "new") {
      store.dispatch(SaveProjectAction(project: _projectModel));
    } else {
      store.dispatch(UpdateProjectAction(project: _projectModel));
    }
  }

  List<PlanningModel> _generatePlanning() {
    List<PlanningModel> response = [];

    var beginDate = _projectModel.maintenance.from;
    var endDate = _projectModel.maintenance.to;

    if (_projectModel.maintenance.from == null ||
        _projectModel.maintenance.to == null ||
        _projectModel.maintenance.cantHours == null) {
      return [];
    }

    endDate = endDate.add(new Duration(days: 30));
    while (beginDate.isBefore(endDate)) {
      response.add(new PlanningModel(
          date: beginDate,
          total: _projectModel.maintenance.cantHours,
          totalPlanning: 0));
      beginDate = beginDate.add(new Duration(days: 31));
    }

    return response;
  }

  Future<bool> _onBackPressed() {
    return showModalBottomSheet(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          context: context,
          builder: (context) => Container(
              height: 250,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 5,
                      left: 5,
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                            onTap: () {
                              Navigator.of(context).pop(false);
                            },
                            child: Icon(Icons.close) // the arrow back icon
                            ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Tienes cambios sin guardar",
                          style: Theme.of(context).textTheme.title,
                        ),
                      ),
                    ),
                    Align(
                        alignment: Alignment.center,
                        child: Container(
                          height: 70,
                          child: Text(
                            "¿Deseas continuar?",
                            style: Theme.of(context).textTheme.title,
                          ),
                        )),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        padding: EdgeInsets.only(bottom: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop(true);
                              },
                              child: Text(
                                "Continuar",
                                style: Theme.of(context).textTheme.button,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            RaisedButton(
                              color: Theme.of(context)
                                  .buttonTheme
                                  .colorScheme
                                  .primary,
                              onPressed: () {
                                Navigator.of(context).pop(false);
                                _onSubmit();
                              },
                              child: Text("Guardar",
                                  style: Theme.of(context).textTheme.button),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )),
        ).then((onValue) {
          return onValue == null ? false : onValue;
        }) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ProjectModel>(
        distinct: true,
        rebuildOnChange: true,
        converter: (store) => store.state.projectsState.selected,
        onInitialBuild: (project) {
          _projectModel = project;
          if(_projectModel.maintenance == null){
            _projectModel.maintenance = new MaintenanceModel();
          }
          // Init title in EditField
          _nameController.text = _projectModel.name;
          setState(() {
            _shadeColor = Color(
                int.parse(_projectModel.shadeColor, radix: 16) + 0xFF000000);
            _tempShadeColor = Color(
                int.parse(_projectModel.shadeColor, radix: 16) + 0xFF000000);
          });


          if (_projectModel.maintenance != null) {
            _fromController.text = _projectModel.maintenance.from != null
                ? DateFormat("yMMM").format(_projectModel.maintenance.from)
                : "";
            _toController.text = _projectModel.maintenance.to != null
                ? DateFormat("yMMM").format(_projectModel.maintenance.to)
                : "";
          }
        },
        builder: (_, project) {

          if(_projectModel == null){
            return Center(child: Loading(),);
          }

          return WillPopScope(
            onWillPop: _onBackPressed,
            child: Scaffold(
                appBar: AppBar(
                  backgroundColor: _tempShadeColor ?? null,
                  title: Text(_projectModel?.name == null || _projectModel?.name?.isEmpty
                      ? "Nuevo Proyecto"
                      : _projectModel.name),
                  actions: <Widget>[
                    Builder(
                      builder: (context) => Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: InkResponse(
                            onTap: () {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                // Si el formulario es válido, queremos mostrar un Snackbar
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content: Text('Guardando los datos...')));
                                _onSubmit();
                              } else {
                                Scaffold.of(context).showSnackBar(SnackBar(
                                    content:
                                        Text('Existen errores en los campos')));
                              }
                            },
                            child: Icon(Icons.save)),
                      ),
                    )
                  ],
                ),
                body: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 15),
                              child: TextFormField(
                                controller: _nameController,
                                focusNode: _nameFocus,
                                textInputAction: TextInputAction.next,
                                textCapitalization: TextCapitalization.words,
                                onChanged: (val) =>
                                    setState(() => _projectModel.name = val),
                                onSaved: (val) =>
                                    setState(() => _projectModel.name = val),
                                onFieldSubmitted: (_) => _fieldFocusChange(
                                    context, _nameFocus, _hourFocus),
                                autofocus: true,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    disabledBorder: InputBorder.none,
                                    hintText: 'Nombre'),
                                // El validator recibe el texto que el usuario ha digitado
                                validator: (value) => value.isEmpty
                                    ? "Entre el nombre del proyecto"
                                    : null,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ListTile(
                              title: Text("Color del proyecto"),
                              trailing: Padding(
                                padding: const EdgeInsets.only(right: 10),
                                child: CircleAvatar(
                                  backgroundColor: _shadeColor,
                                  radius: 15.0,
                                  child: const Text(""),
                                ),
                              ),
                              onTap: _openFullMaterialColorPicker,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            SwitchListTile(
                                title: const Text('Es mantenimiento'),
                                value: _projectModel?.maintenance?.isMaintenaince,
                                onChanged: (bool val) => setState(() =>
                                _projectModel.maintenance.isMaintenaince = val)),
                            SizedBox(
                              height: 20,
                            ),
                            _renderDynamicForm(),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 4),
                                    blurRadius: 30,
                                    color: Colors.grey[200],
                                  ),
                                ],
                              ),
                              child: ListTile(
                                title: Center(
                                    child: Text(
                                      "Planificación",
                                      style: Theme.of(context).textTheme.title,
                                    )),
                                trailing: _projectModel?.maintenance != null &&
                                    _projectModel.maintenance.isMaintenaince
                                    ? null
                                    : Container(
                                  width: 56,
                                  height: 56,
                                  child: ClipOval(
                                    child: Material(
                                      child: InkWell(
                                        onTap: () {
                                          this.addSchedule();
                                        },
                                        child: Icon(
                                          Icons.add_circle,
                                          color: Colors.grey[700],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Expanded(
                              child: _renderList(context),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                )),
          );
        });
  }

  Widget _renderDynamicForm() {
    if (!_projectModel.maintenance.isMaintenaince) {
      return Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: TextFormField(
              initialValue: _projectModel.maintenance?.cantHours?.toString(),
              focusNode: _hourFocus,
              textInputAction: TextInputAction.next,
              validator: (value) =>
              _projectModel.maintenance.isMaintenaince && value.isEmpty
                  ? 'Debe tener unas horas asignadas'
                  : null,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  hintText: 'Total en (hrs)',
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  suffixIcon: Icon(Icons.access_time)),
              onSaved: (value) {
                setState(() => _projectModel.maintenance.cantHours = int.parse(value));
              },
              onEditingComplete: () {
                _formKey.currentState.save();
                if(_projectModel.maintenance.isMaintenaince){
                  var regenerateList = _generatePlanning();
                  setState(() {
                    _projectModel.planning = regenerateList;
                  });
                }

              },
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      );
    }

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: TextFormField(
            initialValue: _projectModel.maintenance?.cantHours?.toString(),
            focusNode: _hourFocus,
            textInputAction: TextInputAction.next,
            validator: (value) =>
                  _projectModel.maintenance.isMaintenaince && value.isEmpty
                    ? 'Debe tener unas horas asignadas'
                    : null,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                hintText: 'Dedicación (hrs)',
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                suffixIcon: Icon(Icons.access_time)),
            onSaved: (value) {
              setState(() => _projectModel.maintenance.cantHours = int.parse(value));
            },
            onEditingComplete: () {
              _formKey.currentState.save();
              if(_projectModel.maintenance.isMaintenaince){
                var regenerateList = _generatePlanning();
                setState(() {
                  _projectModel.planning = regenerateList;
                });
              }
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: TextFormField(
                  controller: _fromController,
                  validator: (value) {
                    if (_projectModel.maintenance.isMaintenaince) {
                      if (value.isEmpty) return "Este valor es requerido";
                      if (_projectModel.maintenance
                          .from
                          .isAfter(_projectModel.maintenance.to))
                        return "Valor incorrecto";
                    }
                    return null;
                  },
                  readOnly: true,
                  decoration: InputDecoration(
                      hintText: 'Desde',
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      suffixIcon: Icon(Icons.calendar_today)),
                  onTap: () => handleReadOnlyInputClick(context, "from"),
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Expanded(
                flex: 1,
                child: TextFormField(
                  controller: _toController,
                  validator: (value) {
                    if (_projectModel.maintenance.isMaintenaince) {
                      if (value.isEmpty) return "Este valor es requerido";
                      if (_projectModel.maintenance
                          .to
                          .isBefore(_projectModel.maintenance.from))
                        return "Valor incorrecto";
                    }
                    return null;
                  },
                  readOnly: true,
                  decoration: InputDecoration(
                      hintText: 'Hasta',
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      suffixIcon: Icon(Icons.calendar_today)),
                  onTap: () => handleReadOnlyInputClick(context, "to"),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  void _openDialog(String title, Widget content) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(6.0),
          title: Center(
            child: Text(title),
          ),
          content: content,
          actions: [
            FlatButton(
              child: Text('Cancelar'),
              onPressed: Navigator.of(context).pop,
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
                setState(() => _mainColor = _tempMainColor);
                setState(() => _shadeColor = _tempShadeColor);
              },
            ),
          ],
        );
      },
    );
  }

  void _openFullMaterialColorPicker() async {
    _openDialog(
      "Seleccione un color",
      MaterialColorPicker(
        colors: fullMaterialColors,
        shrinkWrap: true,
        selectedColor: _mainColor,
        onMainColorChange: (color) => setState(() => _tempMainColor = color),
        onColorChange: (color) => setState(() => _tempShadeColor = color),
      ),
    );
  }

  void handleReadOnlyInputClick(context, source) {
    showMonthPicker(
            context: context,
            firstDate: DateTime(DateTime.now().year - 1, 5),
            lastDate: DateTime(DateTime.now().year + 1, 9),
            initialDate: source == "from"
                ? _projectModel.maintenance.from
                : _projectModel.maintenance.to)
        .then((date) {
      if (date != null) {
        if (source == "from") {
          setState(() => _projectModel.maintenance.from = date);
          _fromController.value = new TextEditingValue(
              text: DateFormat("yMMM").format(_projectModel.maintenance.from));
        } else {
          setState(() => _projectModel.maintenance.to = date);
          _toController.value = new TextEditingValue(
              text: DateFormat("yMMM").format(_projectModel.maintenance.to));
        }
        _projectModel.planning = _generatePlanning();
      }
    });
  }

  void _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  Widget stackBehindDismiss() {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.all(8.0),
      color: Colors.transparent,
      child: Icon(
        Icons.delete,
        color: Colors.grey[700],
      ),
    );
  }

  Widget _renderList(BuildContext context) {
    List<PlanningModel> list = [];
    if (_projectModel.planning != null) {
      list = _projectModel.planning.reversed.toList();
    }

    if(list.length == 0){
      return Container();
    }

    return ListView.builder(
      addAutomaticKeepAlives: true,
      key: ValueKey(new DateTime.now().millisecondsSinceEpoch),
      itemCount:
          _projectModel.planning != null ? _projectModel.planning.length : 0,
      itemBuilder: (BuildContext context, int index) {
        PlanningModel planning = list[index];
        return Dismissible(
          key: ObjectKey(planning),
          background: stackBehindDismiss(),
          onDismissed: (direction) {
            //To delete
            int originalIndex = deleteItem(planning);
            setState(() {
              isDisabled = false;
            });
            flush = Flushbar<bool>(
              message: "Elemento eliminado",
              icon: Icon(
                Icons.undo,
                size: 28.0,
                color: Colors.blue[300],
              ),
              duration: Duration(seconds: 3),
              isDismissible: true,
              showProgressIndicator: true,
              progressIndicatorBackgroundColor: Colors.blueGrey,
              mainButton: FlatButton(
                splashColor: Colors.grey[400],
                onPressed: isDisabled
                    ? null
                    : () {
                        undoDeletion(originalIndex, planning);
                        flush.dismiss(true);
                        setState(() {
                          isDisabled = true;
                        });
                      },
                child: Text(
                  "DESHACER",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              flushbarPosition: FlushbarPosition.BOTTOM,
              flushbarStyle: FlushbarStyle.GROUNDED,
              reverseAnimationCurve: Curves.decelerate,
              forwardAnimationCurve: Curves.bounceIn,
                animationDuration: Duration(milliseconds: 500)
            )..show(context).then((result) {
                setState(() {
                });
              });
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(width: 1.0, color: Colors.grey[500]),
                  color: (planning.date.month == DateTime.now().month &&
                          planning.date.year == DateTime.now().year)
                      ? Colors.grey[300]
                      : null),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: TextFormField(
                      enabled: !_projectModel.maintenance.isMaintenaince,
                      initialValue: planning.date != null
                          ? DateFormat("yMMM").format(planning.date)
                          : "",
                      validator: (value) {
                        if (value.isEmpty) return "Este valor es requerido";
                        return null;
                      },
                      readOnly: true,
                      decoration: InputDecoration(
                          hintText: 'Mes',
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          prefixIcon: Icon(Icons.calendar_today)),
                      onTap: () {
                        showMonthPicker(
                                context: context,
                                firstDate: DateTime(DateTime.now().year - 1, 5),
                                lastDate: DateTime(DateTime.now().year + 1, 9),
                                initialDate: planning.date)
                            .then((date) {
                          if (date != null)
                            setState(() {
                              _projectModel.planning[_projectModel.planning.length-1 - index].date = date;
                            });
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    flex: 1,
                    child: TextFormField(
                      initialValue: planning.total.toString(),
                      textInputAction: TextInputAction.next,
                      validator: (value) => value.isEmpty
                          ? 'Debe tener unas horas asignadas'
                          : null,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          hintText: '(hrs)',
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          suffixIcon: Icon(Icons.access_time)),
                      onSaved: (value) {
                        int indexP = _projectModel.planning.indexWhere((item) =>
                            planning.date.month == item.date.month &&
                            planning.date.year == item.date.year);
                        if (indexP >= 0)
                          _projectModel.planning[indexP].total =
                              int.parse(value);
                      },
                      onEditingComplete: () {
                        _formKey.currentState.save();
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  int deleteItem(PlanningModel planning) {
    int index = _projectModel.planning.indexWhere((item) =>
        planning.date.month == item.date.month &&
        planning.date.year == item.date.year);
    if (index >= 0)
      setState(() {
        _projectModel.planning.removeAt(index);
      });

    return index;
  }

  void undoDeletion(index, item) {
    setState(() {
      _projectModel.planning.insert(index, item);
    });
  }

  void addSchedule(){
    DateTime currentDate = DateTime.now();

    if(_projectModel.planning != null && _projectModel.planning.length >= 1){
      _projectModel.planning.forEach((element) {
        if(element.date.isAfter(currentDate)){
          currentDate = element.date;
        }
      });

      currentDate = currentDate.add(new Duration(days: 31));
    }else{
      _projectModel.planning = [];
    }

    setState(() {
      _projectModel.planning.add(new PlanningModel(date: currentDate, total: 0, totalPlanning: 0));
    });
  }
}
