import 'package:MyPlanning/component/ProjectItemList.dart';
import 'package:MyPlanning/models/Projects.model.dart';
import 'package:MyPlanning/store/actions/project.action.dart';
import 'package:MyPlanning/store/state/app.state.dart';
import 'package:MyPlanning/store/viewModel/ProjectsViewModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class Projects extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ProjectsViewModel>(
        distinct: true,
        rebuildOnChange: true,
        onInit: (store) => store.dispatch(new LoadProjectsAction()),
        converter: (store) => ProjectsViewModel.fromStore(store),
        builder: (_, vm) {
          return ListView.builder(
              itemCount: vm.state.projects.length,
              itemBuilder: (_, int index) {
                ProjectModel item = vm.state.projects[index];
                return ProjectsItem(projectModel: item, onView: vm.onView,);
              });
        });
  }
}
