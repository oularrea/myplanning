// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Users.models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UsersModel _$UsersModelFromJson(Map<String, dynamic> json) {
  return UsersModel(
    users: (json['users'] as List)
        ?.map((e) =>
            e == null ? null : UserModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$UsersModelToJson(UsersModel instance) =>
    <String, dynamic>{
      'users': instance.users,
    };

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return UserModel(
    totalHoursWeek: json['totalHoursWeek'] as int,
    displayName: json['displayName'] as String,
    photoUrl: json['photoUrl'] as String,
    email: json['email'] as String,
    id: json['id'] as String,
  );
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'totalHoursWeek': instance.totalHoursWeek,
      'displayName': instance.displayName,
      'photoUrl': instance.photoUrl,
      'email': instance.email,
      'id': instance.id,
    };
