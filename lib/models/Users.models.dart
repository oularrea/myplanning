import 'package:MyPlanning/models/Projects.model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:random_color/random_color.dart';
import 'package:uuid/uuid.dart';

part 'Users.models.g.dart';

RandomColor _randomColor = RandomColor();

@JsonSerializable()
class UsersModel {
  List<UserModel> users;

  UsersModel({
    @required this.users,
  });

  UsersModel copyWith({
    List<UserModel> users,
  }) =>
      UsersModel(
        users: users ?? this.users,
      );

  factory UsersModel.fromJson(Map<String, dynamic> json) =>
      _$UsersModelFromJson(json);

  Map<String, dynamic> toJson() => _$UsersModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UserModel {
  int totalHoursWeek;
  String displayName;
  String photoUrl;
  String email;
  String id;
  @JsonKey(ignore: true)
  Color color;

  List<ScheduleModel> schedules = [];

  UserModel({
    this.totalHoursWeek = 40,
    this.displayName,
    this.photoUrl,
    this.email,
    this.id,
    this.color,
    this.schedules,
  });

  UserModel copyWith({
    int totalHoursWeek,
    String displayName,
    String photoUrl,
    String email,
    String id,
    Color color,
    List<ScheduleModel> schedules,
  }) =>
      UserModel(
        totalHoursWeek: totalHoursWeek ?? this.totalHoursWeek,
        displayName: displayName ?? this.displayName,
        photoUrl: photoUrl ?? this.photoUrl,
        email: email ?? this.email,
        id: id ?? this.id,
        color: color ?? this.color,
        schedules: schedules ?? this.schedules,
      );

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  factory UserModel.fromSnapshot(DocumentSnapshot d) {
    dynamic data = d.data;
    return new UserModel(
      id: d.documentID,
      displayName: data["displayName"],
      photoUrl: data["photoUrl"],
      email: data["email"],
      totalHoursWeek: data["totalHoursWeek"] as int,
      color: _randomColor.randomColor(),
      schedules: data["schedules"] != null ? List<ScheduleModel>.from(data["schedules"].map((x) => ScheduleModel.fromJson(x))): [],
    );
  }

  List<ScheduleModel> generateSchedule(){
    List<ScheduleModel> scheduleListResult = [];
    if(this.schedules != null)
    scheduleListResult..addAll(this.schedules);

    DateTime currentDate = new DateTime.now();
    DateTime initDate = currentDate.subtract(new Duration(days: 60)); // 2 meses atrás
    DateTime endDate = currentDate.add(new Duration(days:(31 * 6 ))); // 6 meses adelante

    DateTime iterationDate = initDate;
    while(iterationDate.isBefore(endDate)){

      if(iterationDate.day == 1 && iterationDate.isAfter(initDate)){
        var yesterdate = iterationDate.subtract(new Duration(days: 1));
        scheduleListResult.add(new ScheduleModel(createdAt: yesterdate, projectModel: null, id: uuid(), isFirstMonth: true));
      }

      scheduleListResult.add(new ScheduleModel(createdAt: iterationDate, projectModel: null, id: uuid(), isFirstMonth: false));
      iterationDate = iterationDate.add(new Duration(days: 1));
    }

    scheduleListResult.sort((ScheduleModel a, ScheduleModel b){
      if( a.createdAt == b.createdAt) return 0;
      return a.createdAt.isAfter(b.createdAt)? 1: -1;
    });

    return scheduleListResult;
  }
}

String uuid() => new Uuid().v1();
@JsonSerializable(explicitToJson: true)
class ScheduleModel {
  String id;
  DateTime createdAt;
  ProjectModel projectModel;
  int hours = 8;
  bool isFirstMonth = false;

  ScheduleModel({
    @required this.createdAt,
    @required this.projectModel,
    @required this.id,
    this.hours,
    this.isFirstMonth
  });

  ScheduleModel copyWith({
    String createdAt,
    ProjectModel projectModel,
    String id
  }) =>
      ScheduleModel(
        createdAt: createdAt ?? this.createdAt,
        projectModel: projectModel ?? this.projectModel,
        id: id ?? this.id,
          hours: hours ?? this.hours
      );

  factory ScheduleModel.fromJson(Map<String, dynamic> json) => _$ScheduleModelFromJson(json);

  Map<String, dynamic> toJson() => _$ScheduleModelToJson(this);
}