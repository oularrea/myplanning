import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'dart:convert';

part 'Projects.model.g.dart';

@JsonSerializable(explicitToJson: true)
class ProjectsModel {
  List<ProjectModel> projects;

  ProjectsModel({
    @required this.projects,
  });

  ProjectsModel copyWith({
    List<ProjectModel> projectModel,
  }) =>
      ProjectsModel(
        projects: projectModel ?? this.projects,
      );

  factory ProjectsModel.fromRawJson(String str) =>
      ProjectsModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProjectsModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectsModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectsModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class ProjectModel {
  String id;
  DateTime createdAt;
  DateTime updatedAt;
  String name;
  int order;
  String mainColor;
  String shadeColor;
  MaintenanceModel maintenance;

  // @JsonKey(fromJson: _fromGenericJsonList, toJson: _toGenericJsonList) probar luego
  List<PlanningModel> planning = [];

  ProjectModel({
    @required this.id,
    DateTime createdAt,
    DateTime updatedAt,
    this.name = "",
    this.order = 0,
    this.mainColor = "ff546e7a",
    this.shadeColor = "ff607d8b",
    MaintenanceModel maintenance,
    List<PlanningModel> planning,
  })  : createdAt = createdAt ?? new DateTime.now(),
        updatedAt = updatedAt ?? new DateTime.now(),
        planning = planning ?? [],
        maintenance = maintenance ?? new MaintenanceModel();

  ProjectModel copyWith({
    String id,
    DateTime createdAt,
    DateTime updatedAt,
    String name,
    int order,
    String mainColor,
    String shadeColor,
    MaintenanceModel maintenance,
    List<PlanningModel> planning,
  }) =>
      ProjectModel(
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        name: name ?? this.name,
        order: order ?? this.order,
        mainColor: mainColor ?? this.mainColor,
        shadeColor: shadeColor ?? this.shadeColor,
        maintenance: maintenance ?? this.maintenance,
        planning: planning ?? this.planning,
      );

  factory ProjectModel.fromSnapshot(DocumentSnapshot d) {
    dynamic data = d.data;
    return new ProjectModel(
      id: d.documentID,
      name: data["name"],
      mainColor: data["mainColor"],
      shadeColor: data["shadeColor"],
      createdAt: data['createdAt'] == null
          ? null
          : DateTime.parse(data['createdAt'] as String),
      updatedAt: data['updatedAt'] == null
          ? null
          : DateTime.parse(data['updatedAt'] as String),
      order: data["order"],
      maintenance: data["maintenance"] == null
          ? null
          : MaintenanceModel.fromJson(data["maintenance"]),
      planning: List<PlanningModel>.from(
          data["planning"].map((x) => PlanningModel.fromJson(x))),
    );
  }

  factory ProjectModel.fromRawJson(String str) =>
      ProjectModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProjectModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectModelToJson(this);
}

@JsonSerializable(explicitToJson: true)
class MaintenanceModel {
  bool isMaintenaince = false;
  DateTime from;
  DateTime to;
  int cantHours;

  MaintenanceModel({
    bool isMaintenaince,
    DateTime from,
    DateTime to,
    int cantHours,
  })  : from = from ?? new DateTime.now(),
        to = to ?? new DateTime.now(),
        isMaintenaince = isMaintenaince ?? false,
        cantHours = cantHours ?? 0;

  MaintenanceModel copyWith({
    bool isMaintenaince,
    DateTime from,
    DateTime to,
    int cantHours,
  }) =>
      MaintenanceModel(
        isMaintenaince: isMaintenaince ?? this.isMaintenaince,
        from: from ?? this.from,
        to: to ?? this.to,
        cantHours: cantHours ?? this.cantHours,
      );

  factory MaintenanceModel.fromRawJson(String str) =>
      MaintenanceModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MaintenanceModel.fromJson(Map<String, dynamic> json) =>
      _$MaintenanceModelFromJson(json);

  Map<String, dynamic> toJson() => _$MaintenanceModelToJson(this);

  factory MaintenanceModel.fromSnapshot(Map<String, dynamic> json) =>
      MaintenanceModel(
        isMaintenaince: json["isMaintenaince"],
        from: json["from"] != null ? json["from"].toDate() : null,
        to: json["to"] != null ? json["to"].toDate() : null,
        cantHours: json["cantHours"],
      );
}

@JsonSerializable(explicitToJson: true)
class PlanningModel {
  DateTime date;
  int total;
  int totalPlanning;

  PlanningModel({
    @required this.date,
    @required this.total,
    @required this.totalPlanning,
  });

  PlanningModel copyWith({
    DateTime date,
    int total,
    int totalPlanning,
  }) =>
      PlanningModel(
        date: date ?? this.date,
        total: total ?? this.total,
        totalPlanning: totalPlanning ?? this.totalPlanning,
      );

  factory PlanningModel.fromRawJson(String str) =>
      PlanningModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PlanningModel.fromJson(Map<String, dynamic> json) =>
      _$PlanningModelFromJson(json);

  Map<String, dynamic> toJson() => _$PlanningModelToJson(this);

  factory PlanningModel.fromSnapshot(Map<String, dynamic> json) =>
      PlanningModel(
        date: json["date"] != null ? json["date"].toDate() : null,
        total: json["total"],
        totalPlanning: json["totalPlanning"],
      );
}
