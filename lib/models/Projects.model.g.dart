// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Projects.model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectsModel _$ProjectsModelFromJson(Map<String, dynamic> json) {
  return ProjectsModel(
    projects: (json['projects'] as List)
        ?.map((e) =>
            e == null ? null : ProjectModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ProjectsModelToJson(ProjectsModel instance) =>
    <String, dynamic>{
      'projects': instance.projects?.map((e) => e?.toJson())?.toList(),
    };

ProjectModel _$ProjectModelFromJson(Map<String, dynamic> json) {
  return ProjectModel(
    id: json['id'] as String,
    createdAt: json['createdAt'] == null
        ? null
        : DateTime.parse(json['createdAt'] as String),
    updatedAt: json['updatedAt'] == null
        ? null
        : DateTime.parse(json['updatedAt'] as String),
    name: json['name'] as String,
    order: json['order'] as int,
    mainColor: json['mainColor'] as String,
    shadeColor: json['shadeColor'] as String,
    maintenance: json['maintenance'] == null
        ? null
        : MaintenanceModel.fromJson(
            json['maintenance'] as Map<String, dynamic>),
    planning: (json['planning'] as List)
        ?.map((e) => e == null
            ? null
            : PlanningModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ProjectModelToJson(ProjectModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'name': instance.name,
      'order': instance.order,
      'mainColor': instance.mainColor,
      'shadeColor': instance.shadeColor,
      'maintenance': instance.maintenance?.toJson(),
      'planning': instance.planning?.map((e) => e?.toJson())?.toList(),
    };

MaintenanceModel _$MaintenanceModelFromJson(Map<String, dynamic> json) {
  return MaintenanceModel(
    isMaintenaince: json['isMaintenaince'] as bool,
    from: json['from'] == null ? null : DateTime.parse(json['from'] as String),
    to: json['to'] == null ? null : DateTime.parse(json['to'] as String),
    cantHours: json['cantHours'] as int,
  );
}

Map<String, dynamic> _$MaintenanceModelToJson(MaintenanceModel instance) =>
    <String, dynamic>{
      'isMaintenaince': instance.isMaintenaince,
      'from': instance.from?.toIso8601String(),
      'to': instance.to?.toIso8601String(),
      'cantHours': instance.cantHours,
    };

PlanningModel _$PlanningModelFromJson(Map<String, dynamic> json) {
  return PlanningModel(
    date: json['date'] == null ? null : DateTime.parse(json['date'] as String),
    total: json['total'] as int,
    totalPlanning: json['totalPlanning'] as int,
  );
}

Map<String, dynamic> _$PlanningModelToJson(PlanningModel instance) =>
    <String, dynamic>{
      'date': instance.date?.toIso8601String(),
      'total': instance.total,
      'totalPlanning': instance.totalPlanning,
    };
