import 'package:MyPlanning/store/store.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Environment {
  static setup() async {
    // Make sure that the binary messenger binding are properly initialiazed
    WidgetsFlutterBinding.ensureInitialized();

    // lock orientation position
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);

    // transparent status bar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.transparent, //
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
    ));
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return await createStore();
  }
}